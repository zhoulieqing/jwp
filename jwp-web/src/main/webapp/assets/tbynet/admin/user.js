/**
 @Name: user
 */
layui.define(['laypage', 'tbynet'], function(exports) {
	var $ = layui.$
	,form = layui.form
	,layer = layui.layer
	,laytpl = layui.laytpl
	,laypage = layui.laypage
	,tbynet = layui.tbynet;
	
	//表单提交返回成功后才执行这个方法
	tbynet.form['/admin/user/save'] = function(data, required, res){
		layer.msg(res.msg, {
			offset: '15px'
			,icon: 1
			,time: 1000
		}, function(){
			location.href = '/admin/user';
		});
	}; 
	
	tbynet.form['/admin/user/update'] = function(data, required, res){
		layer.msg(res.msg, {
			offset: '15px'
			,icon: 1
			,time: 1000
		}, function(){
			location.href = '/admin/user';
		});
	};
	
	form.on('select(role)', function(data){
		$('input[name=permission]').prop('checked', false);
		tbynet.ajax('/admin/user/permission', {role: data.value}, function(res){
			layui.each(res.data, function(i, e){
				$('#permission_' + e).prop('checked', true);
			});
			form.render();
		});
		form.render();
	}); 
	
    var gather = {
    	search: function(curr, q) {
    		search(curr, q);
    	}
    };
    
    function search(curr, q){
    	if($('#user-page-tpl').length == 0) {
    		return;
    	}
    	
		tbynet.ajax('/admin/user/search', {page: curr || 1, q: q || $('#post-search-input').val()}, function(res){
			
			if(res.data.totalRow == 0) {
				$('table tbody').html('<tr><td colspan="6">找不到用户。</td></tr>');
				$('.tablenav').hide();
			} else {
				laytpl($('#user-page-tpl').html()).render(res.data.list, function(html){
					$('table tbody').html(html);
				});
			}
			
			laypage.render({elem: $('.tablenav-pages'), count: res.data.totalRow, limit: res.data.pageSize, curr: curr || 1, layout: ['count', 'prev', 'page', 'next'], jump: function(obj, first){
				if(!first){
			        search(obj.curr, q);
				}
			}});
			
			form.render();
		}, "json");
	};
	
	search();

    exports('user', gather);
});