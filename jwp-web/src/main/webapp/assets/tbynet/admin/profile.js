/**
 @Name: profile
 */
layui.define(['tbynet'], function(exports) {
	var $ = layui.$
	,form = layui.form
	,tbynet = layui.tbynet;
	
	//表单提交返回成功后才执行这个方法
	tbynet.form['/admin/profile/update'] = function(data, required, res){
		layer.msg(res.msg, {
			offset: '15px'
			,icon: 1
			,time: 1000
		}, function(){
			location.href = '/admin/profile';
		});
	}; 
	
	$('body').on('click', '#destroy-sessions', function(){
		tbynet.ajax('/admin/destroy', {}, function(res) {
			layer.msg('登出其他设备成功', {
				offset: '15px'
				,icon: 1
				,time: 1000
			}, function(){
				
			});
		});
	});

    var gather = {};

    exports('profile', gather);
});