/**
 @Name: attachment
 */
layui.define(['laypage', 'tbynet'], function(exports) {
	var $ = layui.$
	,form = layui.form
	,layer = layui.layer
	,laytpl = layui.laytpl
	,laypage = layui.laypage
	,tbynet = layui.tbynet;
	
	//表单提交返回成功后才执行这个方法
	tbynet.form['/admin/attachment/update'] = function(data, required, res){
		layer.msg(res.msg, {
			offset: '15px'
			,icon: 1
			,time: 1000
		}, function(){
			location.href = '/admin/attachment';
		});
	};
	
    var gather = {
    	search: function(curr, q) {
    		search(curr, q);
    	}
    };
    
    function search(curr, q){
    	if($('#attachment-page-tpl').length == 0) {
    		return;
    	}
    	
		tbynet.ajax('/admin/attachment/search', {page: curr || 1, q: q || $('#post-search-input').val()}, function(res){
			
			if(res.data.totalRow == 0) {
				$('table tbody').html('<tr><td colspan="6">找不到媒体。</td></tr>');
				$('.tablenav').hide();
			} else {
				laytpl($('#attachment-page-tpl').html()).render(res.data.list, function(html){
					$('table tbody').html(html);
				});
			}
			
			laypage.render({elem: $('.tablenav-pages'), count: res.data.totalRow, limit: res.data.pageSize, curr: curr || 1, layout: ['count', 'prev', 'page', 'next'], jump: function(obj, first){
				if(!first){
			        search(obj.curr, q);
				}
			}});
			
			form.render();
		}, "json");
	};
	
	search();

    exports('attachment', gather);
});