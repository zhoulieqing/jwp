/**
 @Name: option
 */
layui.define('tbynet', function(exports) {
	var $ = layui.$
	,form = layui.form
	,tbynet = layui.tbynet;
	
	//表单提交返回成功后才执行这个方法
	tbynet.form['/admin/option/general'] = function(data, required, res){
		layer.msg(res.msg, {
			offset: '15px'
			,icon: 1
			,time: 1000
		}, function(){
			location.href = '/admin/option/general';
		});
	}; 
	
	
	tbynet.form['/admin/option/writing'] = function(data, required, res){
		layer.msg(res.msg, {
			offset: '15px'
			,icon: 1
			,time: 1000
		}, function(){
			location.href = '/admin/option/writing';
		});
	}; 
	
	tbynet.form['/admin/option/reading'] = function(data, required, res){
		layer.msg(res.msg, {
			offset: '15px'
			,icon: 1
			,time: 1000
		}, function(){
			location.href = '/admin/option/reading';
		});
	}; 
	
	tbynet.form['/admin/option/discussion'] = function(data, required, res){
		layer.msg(res.msg, {
			offset: '15px'
			,icon: 1
			,time: 1000
		}, function(){
			location.href = '/admin/option/discussion';
		});
	}; 
	
	tbynet.form['/admin/option/media'] = function(data, required, res){
		layer.msg(res.msg, {
			offset: '15px'
			,icon: 1
			,time: 1000
		}, function(){
			location.href = '/admin/option/media';
		});
	}; 
	
	tbynet.form['/admin/option/permalink'] = function(data, required, res){
		layer.msg(res.msg, {
			offset: '15px'
			,icon: 1
			,time: 1000
		}, function(){
			location.href = '/admin/option/permalink';
		});
	}; 
	
	tbynet.form['/admin/option/privacy'] = function(data, required, res){
		layer.msg(res.msg, {
			offset: '15px'
			,icon: 1
			,time: 1000
		}, function(){
			location.href = '/admin/option/privacy';
		});
	}; 
	
    var gather = {};

    exports('option', gather);
});