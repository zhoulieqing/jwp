package com.tbynet.jwp.web;

import com.jfinal.server.undertow.UndertowServer;
import com.tbynet.jwp.web.config.WebConfig;

public class Application {
	
	public static void main(String[] args) {
		UndertowServer.start(WebConfig.class);
	}

}
