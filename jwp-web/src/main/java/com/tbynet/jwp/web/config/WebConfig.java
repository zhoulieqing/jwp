package com.tbynet.jwp.web.config;

import java.sql.Connection;

import javax.servlet.http.HttpServletRequest;

import com.alibaba.druid.filter.stat.StatFilter;
import com.alibaba.druid.wall.WallFilter;
import com.jfinal.aop.AopManager;
import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.json.MixedJsonFactory;
import com.jfinal.kit.Prop;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.druid.DruidPlugin;
import com.jfinal.plugin.druid.DruidStatViewHandler;
import com.jfinal.plugin.druid.IDruidStatViewAuth;
import com.jfinal.plugin.ehcache.EhCachePlugin;
import com.jfinal.template.Engine;
import com.tbynet.jwp.framework.core.JwpConfig;
import com.tbynet.jwp.framework.directive.CompareDirective;
import com.tbynet.jwp.framework.kit.CookieKit;
import com.tbynet.jwp.model.Users;
import com.tbynet.jwp.model._MappingKit;

public class WebConfig extends JwpConfig {
	
	private static Prop p = loadConfig();
	private WallFilter wallFilter;

	@Override
	public void configConstant(Constants me) {
		me.setDevMode(p.getBoolean("devMode", false));
		me.setJsonFactory(MixedJsonFactory.me());
		me.setError404View("/404.html");
		me.setBaseUploadPath("wp-content/uploads");
		me.setBaseDownloadPath("/");
		me.setInjectDependency(true);
		me.setInjectSuperClass(true);
		
		com.tbynet.jwp.service.provider._MappingKit.mapping(AopManager.me());
	}

	@Override
	public void configEngine(Engine me) {
		//https://www.wpdaxue.com/wordpress-theme-basis-files-and-functions.html
		//https://blog.wpjam.com/article/wp-template-cheatsheet/
		//https://www.lmdouble.com/1032522813.html
		//http://www.zyidai.com/wordpress-mobanhanshu.html
		
		me.addDirective("compare", CompareDirective.class);
		
		me.addDirective("role", com.tbynet.jwp.directive.RoleDirective.class);
		me.addDirective("permission", com.tbynet.jwp.directive.PermissionDirective.class);
		
		// 包含模板文件的标签（Include tags）
		me.addDirective("get_header", com.tbynet.jwp.directive.GetHeaderDirective.class);
		me.addDirective("get_sidebar", com.tbynet.jwp.directive.GetSidebarDirective.class);
		me.addDirective("get_search_form", com.tbynet.jwp.directive.GetSearchFormDirective.class);
		me.addDirective("comments_template", com.tbynet.jwp.directive.CommentsTemplateDirective.class);
		me.addDirective("get_footer", com.tbynet.jwp.directive.GetFooterDirective.class);

		// 博客信息标签（Blog info tags）
		me.addDirective("bloginfo", com.tbynet.jwp.directive.BloginfoDirective.class);
		me.addDirective("bloginfo_rss", com.tbynet.jwp.directive.BloginfoRssDirective.class);
		me.addDirective("get_bloginfo", com.tbynet.jwp.directive.GetBloginfoDirective.class);
		me.addDirective("get_bloginfo_rss", com.tbynet.jwp.directive.GetBloginfoRssDirective.class);

		// 列表及下拉列表的标签（Lists & Dropdown tags）
		me.addDirective("wp_list_authors", com.tbynet.jwp.directive.WpListAuthorsDirective.class);
		me.addDirective("wp_list_categories", com.tbynet.jwp.directive.WpListCategoriesDirective.class);
		me.addDirective("wp_list_pages", com.tbynet.jwp.directive.WpListPagesDirective.class);
		me.addDirective("wp_list_bookmarks", com.tbynet.jwp.directive.WpListBookmarksDirective.class);
		me.addDirective("wp_list_comments", com.tbynet.jwp.directive.WpListCommentsDirective.class);
		me.addDirective("wp_get_archives", com.tbynet.jwp.directive.WpGetArchivesDirective.class);
		me.addDirective("wp_page_menu", com.tbynet.jwp.directive.WpPageMenuDirective.class);
		me.addDirective("wp_dropdown_pages", com.tbynet.jwp.directive.WpDropdownPagesDirective.class);
		me.addDirective("wp_dropdown_categories", com.tbynet.jwp.directive.WpDropdownCategoriesDirective.class);
		me.addDirective("wp_dropdown_users", com.tbynet.jwp.directive.WpDropdownUsersDirective.class);

		// 登录/登出标签（Login/Logout tags）
		me.addDirective("is_user_logged_in", com.tbynet.jwp.directive.IsUserLoggedInDirective.class);
		me.addDirective("wp_login_url", com.tbynet.jwp.directive.WpLoginUrlDirective.class);
		me.addDirective("wp_logout_url", com.tbynet.jwp.directive.WpLogoutUrlDirective.class);
		me.addDirective("wp_lostpassword_url", com.tbynet.jwp.directive.WpLostpasswordUrlDirective.class);
		me.addDirective("wp_registration_url", com.tbynet.jwp.directive.WpRegistrationUrlDirective.class);
		me.addDirective("wp_loginout", com.tbynet.jwp.directive.WpLoginoutDirective.class);
		me.addDirective("wp_register", com.tbynet.jwp.directive.WpRegisterDirective.class);
		me.addDirective("wp_signon", com.tbynet.jwp.directive.WpSignonDirective.class);

		// 文章信息标签（Post tags）
		me.addDirective("the_ID", com.tbynet.jwp.directive.TheIDDirective.class);
		me.addDirective("the_title", com.tbynet.jwp.directive.TheTitleDirective.class);
		me.addDirective("the_title_rss", com.tbynet.jwp.directive.TheTitleRssDirective.class);
		me.addDirective("the_title_attribute", com.tbynet.jwp.directive.TheTitleAttributeDirective.class);
		me.addDirective("single_post_title", com.tbynet.jwp.directive.SinglePostTitleDirective.class);
		me.addDirective("the_content", com.tbynet.jwp.directive.TheContentDirective.class);
		me.addDirective("the_content_rss", com.tbynet.jwp.directive.TheContentRssDirective.class);
		me.addDirective("the_excerpt", com.tbynet.jwp.directive.TheExcerptDirective.class);
		me.addDirective("the_excerpt_rss", com.tbynet.jwp.directive.TheExcerptRssDirective.class);
		me.addDirective("wp_link_pages", com.tbynet.jwp.directive.WpLinkPagesDirective.class);
		me.addDirective("posts_nav_link", com.tbynet.jwp.directive.PostsNavLinkDirective.class);
		me.addDirective("next_post_link", com.tbynet.jwp.directive.NextPostLinkDirective.class);
		me.addDirective("next_posts_link", com.tbynet.jwp.directive.NextPostsLinkDirective.class);
		me.addDirective("previous_post_link", com.tbynet.jwp.directive.PreviousPostLinkDirective.class);
		me.addDirective("previous_posts_link", com.tbynet.jwp.directive.PreviousPostsLinkDirective.class);
		me.addDirective("next_image_link", com.tbynet.jwp.directive.NextImageLinkDirective.class);
		me.addDirective("previous_image_link", com.tbynet.jwp.directive.PreviousImageLinkDirective.class);
		me.addDirective("sticky_class", com.tbynet.jwp.directive.StickyClassDirective.class);
		me.addDirective("the_category", com.tbynet.jwp.directive.TheCategoryDirective.class);
		me.addDirective("the_category_rss", com.tbynet.jwp.directive.TheCategoryRssDirective.class);
		me.addDirective("the_tags", com.tbynet.jwp.directive.TheTagsDirective.class);
		me.addDirective("the_meta", com.tbynet.jwp.directive.TheMetaDirective.class);

		// 评论标签（Comment tags）
//		me.addDirective("wp_list_comments", com.tbynet.wordpress.directive.WpListCommentsDirective.class);
		me.addDirective("comments_number", com.tbynet.jwp.directive.CommentsNumberDirective.class);
		me.addDirective("comments_link", com.tbynet.jwp.directive.CommentsLinkDirective.class);
		me.addDirective("comments_rss_link", com.tbynet.jwp.directive.CommentsRssLinkDirective.class);
		me.addDirective("comments_popup_script", com.tbynet.jwp.directive.CommentsPopupScriptDirective.class);
		me.addDirective("comments_popup_link", com.tbynet.jwp.directive.CommentsPopupLinkDirective.class);
		me.addDirective("comment_ID", com.tbynet.jwp.directive.CommentIDDirective.class);
		me.addDirective("comment_id_fields", com.tbynet.jwp.directive.CommentIdFieldsDirective.class);
		me.addDirective("comment_author", com.tbynet.jwp.directive.CommentAuthorDirective.class);
		me.addDirective("comment_author_link", com.tbynet.jwp.directive.CommentAuthorLinkDirective.class);
		me.addDirective("comment_author_email", com.tbynet.jwp.directive.CommentAuthorEmailDirective.class);
		me.addDirective("comment_author_email_link", com.tbynet.jwp.directive.CommentAuthorEmailLinkDirective.class);
		me.addDirective("comment_author_url", com.tbynet.jwp.directive.CommentAuthorUrlDirective.class);
		me.addDirective("comment_author_url_link", com.tbynet.jwp.directive.CommentAuthorUrlLinkDirective.class);
		me.addDirective("comment_author_IP", com.tbynet.jwp.directive.CommentAuthorIPDirective.class);
		me.addDirective("comment_type", com.tbynet.jwp.directive.CommentTypeDirective.class);
		me.addDirective("comment_text", com.tbynet.jwp.directive.CommentTextDirective.class);
		me.addDirective("comment_excerpt", com.tbynet.jwp.directive.CommentExcerptDirective.class);
		me.addDirective("comment_date", com.tbynet.jwp.directive.CommentDateDirective.class);
		me.addDirective("comment_time", com.tbynet.jwp.directive.CommentTimeDirective.class);
		me.addDirective("comment_form_title", com.tbynet.jwp.directive.CommentFormTitleDirective.class);
		me.addDirective("comment_author_rss", com.tbynet.jwp.directive.CommentAuthorRssDirective.class);
		me.addDirective("comment_text_rss", com.tbynet.jwp.directive.CommentTextRssDirective.class);
		me.addDirective("comment_link_rss", com.tbynet.jwp.directive.CommentLinkRssDirective.class);
		me.addDirective("permalink_comments_rss", com.tbynet.jwp.directive.PermalinkCommentsRssDirective.class);
		me.addDirective("comment_reply_link", com.tbynet.jwp.directive.CommentReplyLinkDirective.class);
		me.addDirective("cancel_comment_reply_link", com.tbynet.jwp.directive.CancelCommentReplyLinkDirective.class);
		me.addDirective("previous_comments_link", com.tbynet.jwp.directive.PreviousCommentsLinkDirective.class);
		me.addDirective("next_comments_link", com.tbynet.jwp.directive.NextCommentsLinkDirective.class);
		me.addDirective("paginate_comments_links", com.tbynet.jwp.directive.PaginateCommentsLinksDirective.class);

		// 时间及日期标签（Date and Time tags）
		me.addDirective("the_time", com.tbynet.jwp.directive.TheTimeDirective.class);
		me.addDirective("the_date", com.tbynet.jwp.directive.TheDateDirective.class);
		me.addDirective("the_date_xml", com.tbynet.jwp.directive.TheDateXmlDirective.class);
		me.addDirective("the_modified_time", com.tbynet.jwp.directive.TheModifiedTimeDirective.class);
		me.addDirective("the_modified_date", com.tbynet.jwp.directive.TheModifiedDateDirective.class);
		me.addDirective("the_modified_author", com.tbynet.jwp.directive.TheModifiedAuthorDirective.class);
		me.addDirective("single_month_title", com.tbynet.jwp.directive.SingleMonthTitleDirective.class);
		me.addDirective("get_the_time", com.tbynet.jwp.directive.GetTheTimeDirective.class);
		me.addDirective("get_day_link", com.tbynet.jwp.directive.GetDayLinkDirective.class);
		me.addDirective("get_month_link", com.tbynet.jwp.directive.GetMonthLinkDirective.class);
		me.addDirective("get_year_link", com.tbynet.jwp.directive.GetYearLinkDirective.class);
		me.addDirective("get_calendar", com.tbynet.jwp.directive.GetCalendarDirective.class);

		// 分类标签（Category tags）
		me.addDirective("is_category", com.tbynet.jwp.directive.IsCategoryDirective.class);
//		me.addDirective("the_category", com.tbynet.wordpress.directive.TheCategoryDirective.class);
//		me.addDirective("the_category_rss", com.tbynet.wordpress.directive.TheCategoryRssDirective.class);
		me.addDirective("single_cat_title", com.tbynet.jwp.directive.SingleCatTitleDirective.class);
		me.addDirective("category_description", com.tbynet.jwp.directive.CategoryDescriptionDirective.class);
//		me.addDirective("wp_dropdown_categories", com.tbynet.wordpress.directive.WpDropdownCategoriesDirective.class);
//		me.addDirective("wp_list_categories", com.tbynet.wordpress.directive.WpListCategoriesDirective.class);
		me.addDirective("get_category_parents", com.tbynet.jwp.directive.GetCategoryParentsDirective.class);
		me.addDirective("get_the_category", com.tbynet.jwp.directive.GetTheCategoryDirective.class);
		me.addDirective("get_category_link", com.tbynet.jwp.directive.GetCategoryLinkDirective.class);
		me.addDirective("in_category", com.tbynet.jwp.directive.InCategoryDirective.class);

		// 作者信息标签（Author tags）
		me.addDirective("the_author", com.tbynet.jwp.directive.TheAuthorDirective.class);
		me.addDirective("the_author_link", com.tbynet.jwp.directive.TheAuthorLinkDirective.class);
		me.addDirective("the_author_posts", com.tbynet.jwp.directive.TheAuthorPostsDirective.class);
		me.addDirective("the_author_posts_link", com.tbynet.jwp.directive.TheAuthorPostsLinkDirective.class);
		me.addDirective("the_author_meta", com.tbynet.jwp.directive.TheAuthorMetaDirective.class);
//		me.addDirective("wp_list_authors", com.tbynet.wordpress.directive.WpListAuthorsDirective.class);
//		me.addDirective("wp_dropdown_users", com.tbynet.wordpress.directive.WpDropdownUsersDirective.class);

		// 文章 Tag 标签（Tag tags）
		me.addDirective("is_tag", com.tbynet.jwp.directive.IsTagDirective.class);
//		me.addDirective("the_tags", com.tbynet.wordpress.directive.TheTagsDirective.class);
		me.addDirective("tag_description", com.tbynet.jwp.directive.TagDescriptionDirective.class);
		me.addDirective("single_tag_title", com.tbynet.jwp.directive.SingleTagTitleDirective.class);
		me.addDirective("wp_tag_cloud", com.tbynet.jwp.directive.WpTagCloudDirective.class);
		me.addDirective("wp_generate_tag_cloud", com.tbynet.jwp.directive.WpGenerateTagCloudDirective.class);
		me.addDirective("get_the_tags", com.tbynet.jwp.directive.GetTheTagsDirective.class);
		me.addDirective("get_the_tag_list", com.tbynet.jwp.directive.GetTheTagListDirective.class);
		me.addDirective("get_tag_link", com.tbynet.jwp.directive.GetTagLinkDirective.class);

		// 编辑链接标签（Edit Link tags）
		me.addDirective("edit_post_link", com.tbynet.jwp.directive.EditPostLinkDirective.class);
		me.addDirective("edit_comment_link", com.tbynet.jwp.directive.EditCommentLinkDirective.class);
		me.addDirective("edit_tag_link", com.tbynet.jwp.directive.EditTagLinkDirective.class);
		me.addDirective("edit_bookmark_link", com.tbynet.jwp.directive.EditBookmarkLinkDirective.class);

		// 固定链接标签（Permalink tags）
		me.addDirective("permalink_anchor", com.tbynet.jwp.directive.PermalinkAnchorDirective.class);
		me.addDirective("get_permalink", com.tbynet.jwp.directive.GetPermalinkDirective.class);
		me.addDirective("the_permalink", com.tbynet.jwp.directive.ThePermalinkDirective.class);
		me.addDirective("permalink_single_rss", com.tbynet.jwp.directive.PermalinkSingleRssDirective.class);

		// 链接管理标签（Links Manager tags）
//		me.addDirective("wp_list_bookmarks", com.tbynet.wordpress.directive.WpListBookmarksDirective.class);
		me.addDirective("get_bookmarks", com.tbynet.jwp.directive.GetBookmarksDirective.class);
		me.addDirective("get_bookmark", com.tbynet.jwp.directive.GetBookmarkDirective.class);
		me.addDirective("get_bookmark_field", com.tbynet.jwp.directive.GetBookmarkFieldDirective.class);

		// 引用标签（Trackback tags）
		me.addDirective("trackback_url", com.tbynet.jwp.directive.TrackbackUrlDirective.class);
		me.addDirective("trackback_rdf", com.tbynet.jwp.directive.TrackbackRdfDirective.class);

		// 一般标签（General tags）
		me.addDirective("wp_title", com.tbynet.jwp.directive.WpTitleDirective.class);
		me.addDirective("get_posts", com.tbynet.jwp.directive.GetPostsDirective.class);
		me.addDirective("query_posts", com.tbynet.jwp.directive.QueryPostsDirective.class);
		me.addDirective("the_search_query", com.tbynet.jwp.directive.TheSearchQueryDirective.class);
	}

	@Override
	public void configHandler(Handlers me) {
		me.add(new DruidStatViewHandler("/druid", new IDruidStatViewAuth() {
			
			@Override
			public boolean isPermitted(HttpServletRequest request) {
				String sessionId = CookieKit.getCookie(request, Users.sessionIdName);
				if (null == sessionId) {
					return false;
				}

				return true;
			}
			
		}));

		me.add(new com.tbynet.jwp.admin.AdminHandler());
		me.add(new com.tbynet.jwp.portal.PortalHandler());
	}

	@Override
	public void configInterceptor(Interceptors me) {
		// TODO Auto-generated method stub

	}

	@Override
	public void configPlugin(Plugins me) {
		DruidPlugin druidPlugin = getDruidPlugin();
	    wallFilter = new WallFilter();              // 加强数据库安全
	    wallFilter.setDbType("mysql");
	    druidPlugin.addFilter(wallFilter);
	    druidPlugin.addFilter(new StatFilter());    // 添加 StatFilter 才会有统计数据
	    me.add(druidPlugin);
	    
	    ActiveRecordPlugin arp = new ActiveRecordPlugin(druidPlugin);
	    arp.setTransactionLevel(Connection.TRANSACTION_READ_COMMITTED);
	    _MappingKit.mapping(arp);
	    me.add(arp);
        if (p.getBoolean("devMode", false)) {
            arp.setShowSql(true);
        }
        
        me.add(new EhCachePlugin());
	}

	@Override
	public void configRoute(Routes me) {
		me.add(new com.tbynet.jwp.admin.AdminRoutes());
		me.add(new com.tbynet.jwp.portal.PortalRoutes());
	}

	@Override
	public void onStart() {
		
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	}

	private static Prop loadConfig() {
		try {
			// 优先加载生产环境配置文件
			return PropKit.use("config.properties");
		} catch (Exception e) {
			// 找不到生产环境配置文件，再去找开发环境配置文件
			return PropKit.use("config.dev.properties");
		}
	}
	
	private static DruidPlugin getDruidPlugin() {
		return new DruidPlugin(p.get("jdbcUrl"), p.get("user"), p.get("password").trim());
	}
}
