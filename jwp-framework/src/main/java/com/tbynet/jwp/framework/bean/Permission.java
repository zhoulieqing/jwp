package com.tbynet.jwp.framework.bean;

public enum Permission {
	
	ActivateRlugins("activate_plugins", "激活插件", "/admin/plugin/activate", ""),
	AddUsers("add_users", "添加用户", "/admin/user/create", ""),
	CreateUsers("create_users", "创建用户", "/admin/user/create", ""),
	DeleteOthersPages("delete_others_pages", "删除他人的页面", "/admin/page/delete", "others"),
	DeleteOthersPosts("delete_others_posts", "删除他人的POSTS", "/admin/post/delete", "others"),
	DeletePages("delete_pages", "删除页面", "/admin/page/delete", ""),
	DeletePlugins("delete_plugins", "删除插件", "/admin/plugin/delete", ""),
	DeletePosts("delete_posts", "删队POSTS", "/admin/post/delete", ""),
	DeletePrivatePages("delete_private_pages", "删除私人的页面", "/admin/page/delete", "private"),
	DeletePrivatePosts("delete_private_posts", "删除私人的POSTS", "/admin/post/delete", "private"),
	DeletePublishedPages("delete_published_pages", "删除已发布的页面", "/admin/page/delete", "published"),
	DeletePublishedPosts("delete_published_posts", "删除已发布的POSTS", "/admin/post/delete", "published"),
	DeleteThemes("delete_themes", "删除主题", "/admin/theme/delete", ""),
	DeleteUsers("delete_users", "删除用户", "/admin/user/delete", ""),
	EditDashboard("edit_dashboard", "编辑控制板", "/admin", ""),
	EditFiles("edit_files", "编辑文件", "/admin/file/edit", ""),
	EditOthersPages("edit_others_pages", "编辑他人的页面", "/admin/page/edit", "others"),
	EditOthersPosts("edit_others_posts", "编辑他人的POSTS", "/admin/post/edit", "others"),
	EditPages("edit_pages", "编辑页面", "/admin/page/edit", ""),
	EditPlugins("edit_plugins", "编辑插件", "/admin/plugin/edit", ""),
	EditPosts("edit_posts", "编辑POSTS", "/admin/post/edit", ""),
	EditPrivatePages("edit_private_pages", "编辑私人的页面", "/admin/page/edit", "private"),
	EditPrivatePosts("edit_private_posts", "编辑私人的POSTS", "/admin/post/edit", "private"),
	EditPublishedPages("edit_published_pages", "编辑已发布的页面", "/admin/page/edit", "published"),
	EditPublishedPosts("edit_published_posts", "编辑已发布的POSTS", "/admin/post/edit", "published"),
	EditThemeOptions("edit_theme_options", "编辑主题选项", "/admin/theme/option/edit", ""),
	EditThemes("edit_themes", "编辑主题", "/admin/theme/edit", ""),
	EditUsers("edit_users", "编辑用户", "/admin/user/edit", ""),
	Export("export", "导出", "/admin/export", ""),
	Import("import", "导入", "/admin/import", ""),
	InstallPlugins("install_plugins", "安装插件", "/admin/plugin/install", ""),
	InstallThemes("install_themes", "安装主题", "/admin/theme/install", ""),
	ListUsers("list_users", "显示用户", "/admin/user", ""),
	ManageCategories("manage_categories", "管理类别", "/admin/category", ""),
	ManageLinks("manage_links", "管理链接", "/admin/link", ""),
	ManageOptions("manage_options", "管理选项", "/admin/option", ""),
	ModerateComments("moderate_comments", "节制评论", "/admin/comment/moderate", ""),
	PromoteUsers("promote_users", "提升用户", "/admin/user/promote", ""),
	PublishPages("publish_pages", "发布页面", "/admin/page/publish", ""),
	PublishPosts("publish_posts", "发布POSTS", "/admin/post/publsh", ""),
	ReadPrivatePages("read_private_pages", "阅读私人的页面", "/admin/page/read", "private"),
	ReadPrivatePosts("read_private_posts", "阅读私人的POSTS", "/admin/post/read", "private"),
	Read("read", "阅读", "/admin", ""),
	RemoveUsers("remove_users", "移除用户", "/admin/user/delete", ""),
	SwitchThemes("switch_themes", "切换主题", "/admin/theme/switch", ""),
	UnfilteredHtml("unfiltered_html", "未过滤的html", "/admin/html", ""), //(not with Multisite. See Unfiltered MU)
	UnfilteredUpload("unfiltered_upload", "未过滤的上传", "/admin/upload", ""),
	UpdateCore("update_core", "更新核心", "/admin/update", ""),
	UpdatePlugins("update_plugins", "更新插件", "/admin/plugin/update", ""),
	UpdateThemes("update_themes", "更新主题", "/admin/plugin/theme", ""),
	UploadFiles("upload_files", "上传文件", "/admin/file/upload", "");
	
	private String name;
	private String displayName;
	private String action;
	private String scope;
	
	private Permission(String name, String displayName, String action, String scope) {
		this.name = name;
		this.displayName = displayName;
		this.action = action;
		this.scope = scope;
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getDisplayName() {
		return this.displayName;
	}
	
	public String getAction() {
		return this.action;
	}
	
	public String getScope() {
		return this.scope;
	}
	
	public static Permission getPermission(String name) {
		for(Permission permission : Permission.values()) {
			if(name.equals(permission.getName())) {
				return permission;
			}
		}
		return null;
	}
	
	public static Permission[] getPermissions() {
		return Permission.values();
	}
}
