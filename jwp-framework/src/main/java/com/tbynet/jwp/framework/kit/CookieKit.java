package com.tbynet.jwp.framework.kit;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

public class CookieKit {

    /**
     * Get cookie value by cookie name.
     */
    public static String getCookie(HttpServletRequest request, String name, String defaultValue) {
        Cookie cookie = getCookieObject(request, name);
        return cookie != null ? cookie.getValue() : defaultValue;
    }

    /**
     * Get cookie value by cookie name.
     */
    public static String getCookie(HttpServletRequest request, String name) {
        return getCookie(request, name, null);
    }

    /**
     * Get cookie value by cookie name and convert to Integer.
     */
    public static Integer getCookieToInt(HttpServletRequest request, String name) {
        String result = getCookie(request, name);
        return result != null ? Integer.parseInt(result) : null;
    }

    /**
     * Get cookie value by cookie name and convert to Integer.
     */
    public static Integer getCookieToInt(HttpServletRequest request, String name, Integer defaultValue) {
        String result = getCookie(request, name);
        return result != null ? Integer.parseInt(result) : defaultValue;
    }

    /**
     * Get cookie value by cookie name and convert to Long.
     */
    public static Long getCookieToLong(HttpServletRequest request, String name) {
        String result = getCookie(request, name);
        return result != null ? Long.parseLong(result) : null;
    }

    /**
     * Get cookie value by cookie name and convert to Long.
     */
    public static Long getCookieToLong(HttpServletRequest request, String name, Long defaultValue) {
        String result = getCookie(request, name);
        return result != null ? Long.parseLong(result) : defaultValue;
    }

    /**
     * Get cookie object by cookie name.
     */
    public static Cookie getCookieObject(HttpServletRequest request, String name) {
        Cookie[] cookies = request.getCookies();
        if (cookies != null)
            for (Cookie cookie : cookies)
                if (cookie.getName().equals(name))
                    return cookie;
        return null;
    }

}
