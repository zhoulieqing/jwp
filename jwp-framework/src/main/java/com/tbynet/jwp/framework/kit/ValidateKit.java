package com.tbynet.jwp.framework.kit;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.jfinal.kit.StrKit;

public class ValidateKit {
	
	/*
	 * 验证字符
	 */
	public static boolean validate(String str, String regex) {
		if(StrKit.isBlank(str)) {
			return false;
		}
		
		Pattern p = Pattern.compile(regex);
		Matcher m = p.matcher(str);
		
		return m.matches();
	}

	/*
	 * 验证密码
	 */
	public static boolean validatePassword(String password) {
		return validate(password, "^[a-zA-Z0-9]{6,16}$");
	}
	
	/*
	 * 验证邮箱
	 */
	public static boolean validateEmail(String email) {
		return validate(email, "^([a-z0-9A-Z]+[-|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$");
	}
	
	/*
	 * 验证手机号
	 */
	public static boolean validateMobile(String mobile) {
		return validate(mobile, "^[1][3,4,5,7,8][0-9]{9}$");
	}
	
	/*
	 * 验证电话号码
	 */
	public static boolean validatePhone(String phone) {
		if(StrKit.isBlank(phone)) {
			return false;
		}
		
		if (phone.length() > 9) {
			return validate(phone, "^[0][1-9]{2,3}-[0-9]{5,10}$");// 验证带区号的 
		}
		
		return validate(phone, "^[1-9]{1}[0-9]{5,8}$");// 验证没有区号的
	}
	
	/*
	 * 验证身份证
	 */
	public static boolean validateIdCard(String idCard) {
		return validate(idCard, "(^\\d{18}$)|(^\\d{15}$)");
	}
	
	/*
	 * 验证URL
	 */
	public static boolean validateUrl(String url) {
		return validate(url, "http(s)?://([\\w-]+\\.)+[\\w-]+(/[\\w- ./?%&=]*)?");
	}
}
