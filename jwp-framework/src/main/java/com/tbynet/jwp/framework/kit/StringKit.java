package com.tbynet.jwp.framework.kit;

import java.net.URLDecoder;
import java.net.URLEncoder;

public class StringKit {

	//截取字符串
	public static String substring(String str, int length) {
		if (str.length() < length / 2)
			return str;
		int count = 0;
		StringBuffer sb = new StringBuffer();
		String[] ss = str.split("");
		for (int i = 1; i < ss.length; i++) {
			count += ss[i].getBytes().length > 1 ? 2 : 1;
			sb.append(ss[i]);
			if (count >= length)
				break;
		}
		// 不需要显示...的可以直接return sb.toString();
		return (sb.toString().length() < str.length()) ? sb.append("...").toString() : str;
	}
	
	//字符串解码
	public static String urldecode(String str) throws Exception {
		return URLDecoder.decode(str, "utf-8");
	}
	
	//字符串加码
	public static String urlencode(String str) throws Exception {
		return URLEncoder.encode(str, "utf-8");
	}
	
}
