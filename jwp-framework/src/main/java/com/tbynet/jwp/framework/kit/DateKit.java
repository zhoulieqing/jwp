package com.tbynet.jwp.framework.kit;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.jfinal.kit.StrKit;

public class DateKit {
	
	private static final long ONE_MINUTE = 60000L;
    private static final long ONE_HOUR = 3600000L;
    private static final long ONE_DAY = 86400000L;
    private static final long ONE_WEEK = 604800000L;
 
    private static final String ONE_SECOND_AGO = "秒前";
    private static final String ONE_MINUTE_AGO = "分钟前";
    private static final String ONE_HOUR_AGO = "小时前";
    private static final String ONE_DAY_AGO = "天前";
    private static final String ONE_MONTH_AGO = "月前";
    private static final String ONE_YEAR_AGO = "年前";
    
    public static String format(Date date, String pattern) {
    	long delta = new Date().getTime() - date.getTime();
        if (delta < 1L * ONE_MINUTE) {
            long seconds = toSeconds(delta);
            return (seconds <= 0 ? 1 : seconds) + ONE_SECOND_AGO;
        }
        if (delta < 45L * ONE_MINUTE) {
            long minutes = toMinutes(delta);
            return (minutes <= 0 ? 1 : minutes) + ONE_MINUTE_AGO;
        }
        if (delta < 24L * ONE_HOUR) {
            long hours = toHours(delta);
            return (hours <= 0 ? 1 : hours) + ONE_HOUR_AGO;
        }
        
        return toStr(date, pattern);
    }
    
    public static String format(Date date) {
        long delta = new Date().getTime() - date.getTime();
        if (delta < 1L * ONE_MINUTE) {
            long seconds = toSeconds(delta);
            return (seconds <= 0 ? 1 : seconds) + ONE_SECOND_AGO;
        }
        if (delta < 45L * ONE_MINUTE) {
            long minutes = toMinutes(delta);
            return (minutes <= 0 ? 1 : minutes) + ONE_MINUTE_AGO;
        }
        if (delta < 24L * ONE_HOUR) {
            long hours = toHours(delta);
            return (hours <= 0 ? 1 : hours) + ONE_HOUR_AGO;
        }
        if (delta < 48L * ONE_HOUR) {
            return "昨天";
        }
        if (delta < 30L * ONE_DAY) {
            long days = toDays(delta);
            return (days <= 0 ? 1 : days) + ONE_DAY_AGO;
        }
        if (delta < 12L * 4L * ONE_WEEK) {
            long months = toMonths(delta);
            return (months <= 0 ? 1 : months) + ONE_MONTH_AGO;
        } else {
            long years = toYears(delta);
            return (years <= 0 ? 1 : years) + ONE_YEAR_AGO;
        }
    }
    
    public static String datePattern = "yyyy-MM-dd";
	public static String timeStampPattern = "yyyy-MM-dd HH:mm:ss";
	
	public static void setDatePattern(String datePattern) {
		if (StrKit.isBlank(datePattern)) {
			throw new IllegalArgumentException("datePattern can not be blank");
		}
		DateKit.datePattern = datePattern;
	}
	
	public static void setTimeStampPattern(String timeStampPattern) {
		if (StrKit.isBlank(timeStampPattern)) {
			throw new IllegalArgumentException("timeStampPattern can not be blank");
		}
		DateKit.timeStampPattern = timeStampPattern;
	}
	
	public static Date toDate(String dateStr) {
		if (StrKit.isBlank(dateStr)) {
			return null;
		}
		
		dateStr = dateStr.trim();
		int length = dateStr.length();
		try {
			if (length == timeStampPattern.length()) {
				SimpleDateFormat sdf = new SimpleDateFormat(timeStampPattern);
				try {
					return sdf.parse(dateStr);
				} catch (ParseException e) {
					dateStr = dateStr.replace(".", "-");
					dateStr = dateStr.replace("/", "-");
					return sdf.parse(dateStr);
				}
			} else if (length == datePattern.length()) {
				SimpleDateFormat sdfDate = new SimpleDateFormat(datePattern);
				try {
					return sdfDate.parse(dateStr);
				} catch (ParseException e) {
					dateStr = dateStr.replace(".", "-");
					dateStr = dateStr.replace("/", "-");
					return sdfDate.parse(dateStr);
				}
			} else {
				throw new IllegalArgumentException("The date format is not supported for the time being");
			}
		} catch (ParseException e) {
			throw new IllegalArgumentException("The date format is not supported for the time being");
		}
	}
	
	public static String toStr(Date date) {
		return toStr(date, DateKit.datePattern);
	}
	
	public static String toStr(Date date, String pattern) {
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		return sdf.format(date);
	}
 
    private static long toSeconds(long date) {
        return date / 1000L;
    }
 
    private static long toMinutes(long date) {
        return toSeconds(date) / 60L;
    }
 
    private static long toHours(long date) {
        return toMinutes(date) / 60L;
    }
 
    private static long toDays(long date) {
        return toHours(date) / 24L;
    }
 
    private static long toMonths(long date) {
        return toDays(date) / 30L;
    }
 
    private static long toYears(long date) {
        return toMonths(date) / 365L;
    }

}
