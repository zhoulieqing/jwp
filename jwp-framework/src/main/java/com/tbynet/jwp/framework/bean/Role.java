package com.tbynet.jwp.framework.bean;

import java.util.Arrays;

public enum Role {

	Subscriber("subscriber", Permission.Read), //订阅者
	Contributor("contributor", Permission.DeletePosts, Permission.EditPosts, Permission.Read), //投稿者
	Author("author", Permission.DeletePosts, 
			Permission.DeletePublishedPosts, 
			Permission.EditPosts, 
			Permission.EditPublishedPosts, 
			Permission.PublishPosts, 
			Permission.Read, 
			Permission.UploadFiles), //作者
	Editor("editor", Permission.DeleteOthersPages, 
			Permission.DeleteOthersPosts, 
			Permission.DeletePages, 
			Permission.DeletePosts, 
			Permission.DeletePrivatePages, 
			Permission.DeletePrivatePosts, 
			Permission.DeletePublishedPages,
			Permission.DeletePublishedPosts,
			Permission.EditOthersPages,
			Permission.EditOthersPosts,
			Permission.EditPages,
			Permission.EditPosts,
			Permission.EditPrivatePages,
			Permission.EditPrivatePosts,
			Permission.EditPublishedPages,
			Permission.EditPublishedPosts,
			Permission.ManageCategories,
			Permission.ManageLinks,
			Permission.ModerateComments,
			Permission.PublishPages,
			Permission.PublishPosts,
			Permission.Read,
			Permission.ReadPrivatePages,
			Permission.ReadPrivatePosts,
			Permission.UnfilteredHtml,
			Permission.UploadFiles), //编辑
	Administrator("administrator", Permission.ActivateRlugins,
			Permission.AddUsers,
			Permission.CreateUsers,
			Permission.DeleteOthersPages,
			Permission.DeleteOthersPosts,
			Permission.DeletePages,
			Permission.DeletePlugins,
			Permission.DeletePosts,
			Permission.DeletePrivatePages,
			Permission.DeletePrivatePosts,
			Permission.DeletePublishedPages,
			Permission.DeletePublishedPosts,
			Permission.DeleteThemes,
			Permission.DeleteUsers,
			Permission.EditDashboard,
			Permission.EditFiles,
			Permission.EditOthersPages,
			Permission.EditOthersPosts,
			Permission.EditPages,
			Permission.EditPlugins,
			Permission.EditPosts,
			Permission.EditPrivatePages,
			Permission.EditPrivatePosts,
			Permission.EditPublishedPages,
			Permission.EditPublishedPosts,
			Permission.EditThemeOptions,
			Permission.EditThemes,
			Permission.EditUsers,
			Permission.Export,
			Permission.Import,
			Permission.InstallPlugins,
			Permission.InstallThemes,
			Permission.ListUsers,
			Permission.ManageCategories,
			Permission.ManageLinks,
			Permission.ManageOptions,
			Permission.ModerateComments,
			Permission.PromoteUsers,
			Permission.PublishPages,
			Permission.PublishPosts,
			Permission.Read,
			Permission.ReadPrivatePages,
			Permission.ReadPrivatePosts,
			Permission.RemoveUsers,
			Permission.SwitchThemes,
			Permission.UnfilteredHtml,
			Permission.UnfilteredUpload,
			Permission.UpdateCore,
			Permission.UpdatePlugins,
			Permission.UpdateThemes,
			Permission.UploadFiles); //管理员
	
	private String name;
	private Permission[] permissions;
	
	private Role(String name, Permission...permissions) {
		this.name = name;
		this.permissions = permissions;
	}
	
	public String getName() {
		return this.name;
	}
	
	public Permission[] getPermissions() {
		return this.permissions;
	}
	
	public static Role getRole(String name) {
		for(Role role : Role.values()) {
			if(name.equals(role.getName())) {
				return role;
			}
		}
		return null;
	}
	
	public static Permission[] getPermissions(String name) {
		return getRole(name).getPermissions();
	}
	
	public boolean hasPermission(Permission permission) {
		return Arrays.asList(this.permissions).contains(permission);
	}
	
	public boolean hasPermission(String permission) {
		return hasPermission(Permission.getPermission(permission));
	}
	
}
