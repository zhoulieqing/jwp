package com.tbynet.jwp.framework.directive;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.jfinal.template.Directive;
import com.jfinal.template.Env;
import com.jfinal.template.expr.ast.Expr;
import com.jfinal.template.expr.ast.ExprList;
import com.jfinal.template.io.Writer;
import com.jfinal.template.stat.ParseException;
import com.jfinal.template.stat.Scope;

public class CompareDirective extends Directive {
	
	private Expr mapExpr;
	
	@Override
	public void exec(Env env, Scope scope, Writer writer) {
		Object map = mapExpr.eval(scope);
		
//		Kv kv = Kv.create();
//		
//		for(Expr expr : exprList.getExprArray()) {
//			Assign obj = (Assign)expr;
//			kv.set(obj.getId().toLowerCase(), obj.eval(scope));
//		}
		
		Map<String, List<?>> temp = (Map<String, List<?>>) map;
		Iterator<String> it = temp.keySet().iterator();
		while (it.hasNext()) {
			String key = it.next();
			
			
		}
		
		scope.set("", map);
		stat.exec(env, scope, writer);
	}
	
	@Override
	public void setExprList(ExprList exprList) {
		if(1 != exprList.length()) {
			throw new ParseException("Wrong number parameter of #image directive, one parameters allowed at most", location);
		}
		
		this.mapExpr = exprList.getExprArray()[0];
	}

	@Override
	public boolean hasEnd() {
		return true;
	}

}
