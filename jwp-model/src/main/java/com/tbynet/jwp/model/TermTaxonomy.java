package com.tbynet.jwp.model;

import com.tbynet.jwp.model.base.BaseTermTaxonomy;

/**
 * 分类法
 * @ClassName: TermTaxonomy
 * @Description: linked to TermRelationships with term_taxonomy_id
 * @Author: 佰亿互联
 * @Date: 2019年3月21日 下午11:54:39
 * 
 * @Copyright: 2019 www.tbynet.com Inc. All rights reserved.
 * 注意: 本内容仅限于内部传阅，禁止外泄以及用于其他的商业目
 */
@SuppressWarnings("serial")
public class TermTaxonomy extends BaseTermTaxonomy<TermTaxonomy> {
	
//	目录（category）
//	标签（tag）
//	链接目录（link category）
	
	public static final String TAXONOMY_CATEGORY = "category"; //分类
	public static final String TAXONOMY_POST_TAG = "post_tag"; //标签
	public static final String TAXONOMY_NAV_MENU = "nav_menu"; //菜单
	public static final String TAXONOMY_POST_FORMAT = "post_format"; //形式
	
}
