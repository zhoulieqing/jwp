package com.tbynet.jwp.model;

import org.phprpc.util.PHPSerializer;

import com.jfinal.kit.Kv;
import com.jfinal.kit.StrKit;
import com.tbynet.jwp.model.base.BasePostmeta;

/**
 * 文章的元数据
 * @ClassName: Postmeta
 * @Description: linked to Posts with post_id
 * @Author: 佰亿互联
 * @Date: 2019年3月22日 上午12:28:26
 * 
 * @Copyright: 2019 www.tbynet.com Inc. All rights reserved.
 * 注意: 本内容仅限于内部传阅，禁止外泄以及用于其他的商业目
 */
@SuppressWarnings("serial")
public class Postmeta extends BasePostmeta<Postmeta> {
	
	public static final String _customize_changeset_uuid = "_customize_changeset_uuid"; //e8152489-9bf8-4172-9c5d-cc3eaaf73693	
	public static final String _customize_draft_post_name = "_customize_draft_post_name"; //%e6%b5%93%e7%bc%a9%e5%92%96%e5%95%a1	
	public static final String _customize_restore_dismissed = "_customize_restore_dismissed"; //1	
	public static final String _edit_last = "_edit_last"; //1	
	public static final String _edit_lock = "_edit_lock"; //1522679286:1	
	public static final String _menu_item_classes = "_menu_item_classes"; //a:1:{i:0;s:0:"";}	
	public static final String _menu_item_menu_item_parent = "_menu_item_menu_item_parent"; //0	
	public static final String _menu_item_object = "_menu_item_object"; //post	
	public static final String _menu_item_object_id = "_menu_item_object_id"; //1	
	public static final String _menu_item_target = "_menu_item_target"; //
	public static final String _menu_item_type = "_menu_item_type"; //post_type	
	public static final String _menu_item_url = "_menu_item_url"; //
	public static final String _menu_item_xfn = "_menu_item_xfn"; //
	public static final String _starter_content_theme = "_starter_content_theme"; //twentyseventeen	
	public static final String _thumbnail_id = "_thumbnail_id"; //
	public static final String _wp_attached_file = "_wp_attached_file"; //2019/03/espresso.jpg	
	public static final String _wp_attachment_metadata = "_wp_attachment_metadata"; //a:5:{s:5:"width";i:2000;s:6:"height";i:1200;s:4:"f...	
	public static final String _wp_page_template = "_wp_page_template"; //default	

	public void setThumbnailId(String value) {
		setMetaKey(_thumbnail_id);
		setMetaValue(value);
	}
	
	public void setWpAttachedFile(String value) {
		setMetaKey(_wp_attached_file);
		setMetaValue(value);
	}
	
	public void setWpAttachmentMetadata(Kv kv) throws Exception {
		PHPSerializer p = new PHPSerializer();
		
		setMetaKey(_wp_attachment_metadata);
		setMetaValue(new String(p.serialize(kv)));
	}
	
	public Object getWpAttachmentMetadata() throws Exception {
//		数据库存储格式
//		a:5:{
//			s:5:"width";i:500;s:6:"height";i:200;s:4:"file";s:8:"bb-2.jpg";
//			s:5:"sizes";a:3:{
//				s:9:"thumbnail";a:4:{
//					s:4:"file";s:16:"bb-2-375x150.jpg";s:5:"width";i:375;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";
//				}
//				s:6:"medium";a:4:{
//					s:4:"file";s:16:"bb-2-300x120.jpg";s:5:"width";i:300;s:6:"height";i:120;s:9:"mime-type";s:10:"image/jpeg";
//				}
//				s:32:"large";a:4:{
//					s:4:"file";s:16:"bb-2-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";
//				}
//			}
//			s:10:"image_meta";a:12:{
//				s:8:"aperture";s:1:"0";
//				s:6:"credit";s:0:"";
//				s:6:"camera";s:0:"";
//				s:7:"caption";s:0:"";
//				s:17:"created_timestamp";s:1:"0";
//				s:9:"copyright";s:0:"";
//				s:12:"focal_length";s:1:"0";
//				s:3:"iso";s:1:"0";
//				s:13:"shutter_speed";s:1:"0";
//				s:5:"title";s:0:"";
//				s:11:"orientation";s:1:"0";
//				s:8:"keywords";a:0:{}
//			}
//		}
		
		if (StrKit.isBlank(getMetaValue()))
	        return new Object();
	    
	    PHPSerializer p = new PHPSerializer();
	    
	    return p.unserialize(getMetaValue().getBytes());
	}
}
