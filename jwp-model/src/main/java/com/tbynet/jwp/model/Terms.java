package com.tbynet.jwp.model;

import com.tbynet.jwp.model.base.BaseTerms;

/**
 * 分类项
 * @ClassName: Terms
 * @Description: linked to TermTaxonomy with term_id
 * @Author: 佰亿互联
 * @Date: 2019年3月21日 下午11:54:55
 * 
 * @Copyright: 2019 www.tbynet.com Inc. All rights reserved.
 * 注意: 本内容仅限于内部传阅，禁止外泄以及用于其他的商业目
 */
@SuppressWarnings("serial")
public class Terms extends BaseTerms<Terms> {
	
}
