package com.tbynet.jwp.model;

import com.tbynet.jwp.model.base.BasePosts;

/**
 * 文章
 * @ClassName: Posts
 * @Description: linked to Postmeta with post_id, TermRelationships with post_id
 * @Author: 佰亿互联
 * @Date: 2019年3月22日 上午12:27:29
 * 
 * @Copyright: 2019 www.tbynet.com Inc. All rights reserved.
 * 注意: 本内容仅限于内部传阅，禁止外泄以及用于其他的商业目
 */
@SuppressWarnings("serial")
public class Posts extends BasePosts<Posts> {
	
	/*
	 * 内容状态
	 */
	public static final String POST_STATUS_PUBLISH = "publish"; //公开的
	public static final String POST_STATUS_FUTURE = "future"; //定时
	public static final String POST_STATUS_DRAFT = "draft"; //草稿
	public static final String POST_STATUS_PENDING = "pending"; //待审核
	public static final String POST_STATUS_PRIVATE = "private"; //私有的
	public static final String POST_STATUS_TRASH = "trash"; //垃圾箱
	public static final String POST_STATUS_AUTO_DRAFT = "auto-draft"; //自动草稿
	public static final String POST_STATUS_INHERIT = "inherit"; //继承，子页面继承父级页面的属性
	
	/*
	 * 评论状态
	 */
	public static final String COMMENT_STATUS_OPEN = "open"; //开启
	public static final String COMMENT_STATUS_CLOSED = "closed"; //关闭
	
	/*
	 * Ping（更新通知）服务状态
	 */
	public static final String PING_STATUS_OPEN = "open"; //开启
	public static final String PING_STATUS_CLOSED = "closed"; //关闭
	
	/*
	 * 内容类型
	 */
	public static final String POST_TYPE_POST = "post"; //文章
	public static final String POST_TYPE_PAGE = "page"; //页面
	public static final String POST_TYPE_ATTACHMENT = "attachment"; //附件
	public static final String POST_TYPE_REVISION = "revision"; //修订版本
	public static final String POST_TYPE_NAV_MENU_ITEM = "nav_menu_item"; //导航菜单项
	
	
	public static final String POST_FORMAT_ASIDE = "aside"; //日志
	public static final String POST_FORMAT_IMAGE = "image"; //图像
	public static final String POST_FORMAT_VIDEO = "video"; //视频
	public static final String POST_FORMAT_QUOTE = "quote"; //引语
	public static final String POST_FORMAT_LINK = "link"; //链接
	public static final String POST_FORMAT_GALLERY = "gallery"; //相册
	public static final String POST_FORMAT_AUDIO = "audio"; //音频
	
}
