package com.tbynet.jwp.model;

import com.tbynet.jwp.model.base.BaseComments;

/**
 * 评论
 * @ClassName: Comments
 * @Description: linked to Posts with post_id
 * @Author: 佰亿互联
 * @Date: 2019年3月22日 上午12:20:32
 * 
 * @Copyright: 2019 www.tbynet.com Inc. All rights reserved.
 * 注意: 本内容仅限于内部传阅，禁止外泄以及用于其他的商业目
 */
@SuppressWarnings("serial")
public class Comments extends BaseComments<Comments> {

    public static final String COMMENT_STATUS_MODERATED = "0";//待审: 0
    public static final String COMMENT_STATUS_APPROVED = "1";//批准: 1
    public static final String COMMENT_STATUS_SPAM = "spam";//垃圾
    public static final String COMMENT_STATUS_TRASH = "trash";//回收站

    public boolean isModerated() {
        return getCommentApproved().equals(COMMENT_STATUS_MODERATED);
    }

    public boolean isApproved() {
        return getCommentApproved().equals(COMMENT_STATUS_APPROVED);
    }

    public boolean isSpam() {
        return getCommentApproved().equals(COMMENT_STATUS_SPAM);
    }

    public boolean isTrash() {
        return getCommentApproved().equals(COMMENT_STATUS_TRASH);
    }

    public static String assembleCommentApprovedClauses(String status) {
        String clauses = null;
        switch (status) {
            case "hold":
                clauses = "comment_approved = '0'";
                break;
            case "approve":
                clauses = "comment_approved = '1'";
                break;
            case "all":
            case "":
                clauses = "( comment_approved = '0' OR comment_approved = '1' )";
                break;
            default:
                clauses = String.format("comment_approved = %s", status);
                break;
        }

        return clauses;
    }
}
