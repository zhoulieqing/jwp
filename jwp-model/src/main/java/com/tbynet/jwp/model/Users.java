package com.tbynet.jwp.model;

import java.util.ArrayList;
import java.util.List;

import com.tbynet.jwp.model.base.BaseUsers;

/**
 * 用户
 * @ClassName: Users
 * @Description: linked to Posts with post_author, Comments with user_id
 * @Author: 佰亿互联
 * @Date: 2019年3月22日 上午12:22:56
 * 
 * @Copyright: 2019 www.tbynet.com Inc. All rights reserved.
 * 注意: 本内容仅限于内部传阅，禁止外泄以及用于其他的商业目
 */
@SuppressWarnings("serial")
public class Users extends BaseUsers<Users> {
	
	List<Usermeta> metas = new ArrayList<Usermeta>();
	
	// 存放登录用户的 cacheName
	public static final String loginUserCacheName = "loginUser";

	// "tbynetId" 仅用于 cookie 名称，其它地方如 cache 中全部用的 "sessionId" 来做 key
	public static final String sessionIdName = "tbynetId";
	
	
	public Users setNickname(String nickname) {
		Usermeta meta = new Usermeta();
		meta.setNickname(nickname);
		meta.setUserId(getID());
		metas.add(meta);
		return this;
	}
	
	public Users setFirstName(String firstName) {
		Usermeta meta = new Usermeta();
		meta.setFirstName(firstName);
		meta.setUserId(getID());
		metas.add(meta);
		return this;
	}
	
	public Users setLastName(String lastName) {
		Usermeta meta = new  Usermeta();
		meta.setLastName(lastName);
		meta.setUserId(getID());
		metas.add(meta);
		return this;
	}
	
	public Users setDescription(String description) {
		Usermeta meta = new Usermeta();
		meta.setDescription(description);
		meta.setUserId(getID());
		metas.add(meta);
		return this;
	}
	
	public Users setRichEditing(String richEditing) {
		Usermeta meta = new Usermeta();
		meta.setRichEditing(richEditing);
		meta.setUserId(getID());
		metas.add(meta);
		return this;
	}
	
	public Users setSyntaxHighlighting(String syntaxHighlighting) {
		Usermeta meta = new Usermeta();
		meta.setSyntaxHighlighting(syntaxHighlighting);
		meta.setUserId(getID());
		metas.add(meta);
		return this;
	}
	
	public Users setCommentShortcuts(String commentShortcuts) {
		Usermeta meta = new Usermeta();
		meta.setCommentShortcuts(commentShortcuts);
		meta.setUserId(getID());
		metas.add(meta);
		return this;
	}
	
	public Users setAdminColor(String adminColor) {
		Usermeta meta = new Usermeta();
		meta.setAdminColor(adminColor);
		meta.setUserId(getID());
		metas.add(meta);
		return this;
	}
	
	public Users setUseSSL(String useSSL) {
		Usermeta meta = new Usermeta();
		meta.setUseSSL(useSSL);
		meta.setUserId(getID());
		metas.add(meta);
		return this;
	}
	
	public Users setShowAdminBarFront(String showAdminBarFront) {
		Usermeta meta = new Usermeta();
		meta.setShowAdminBarFront(showAdminBarFront);
		meta.setUserId(getID());
		metas.add(meta);
		return this;
	}
	
	public Users setLocale(String locale) {
		Usermeta meta = new Usermeta();
		meta.setLocale(locale);
		meta.setUserId(getID());
		metas.add(meta);
		return this;
	}
	
	public Users setRole(String role) {
		Usermeta meta = new Usermeta();
		meta.setRole(role);
		meta.setUserId(getID());
		metas.add(meta);
		return this;
	}
	
	public Users setPermissions(Object...permissions) throws Exception {
		Usermeta meta = new Usermeta();
		meta.setPermissions(permissions);
		meta.setUserId(getID());
		metas.add(meta);
		return this;
	}
	
	public Users setWpCapabilities(Object...capabilities) throws Exception {
		Usermeta meta = new Usermeta();
		meta.setWpCapabilities(capabilities);
		meta.setUserId(getID());
		metas.add(meta);
		return this;
	}
	
	public Users setWpUserLevel(String wpUserLevel) {
		Usermeta meta = new Usermeta();
		meta.setWpUserLevel(wpUserLevel);
		meta.setUserId(getID());
		metas.add(meta);
		return this;
	}
	
	public Users setDismissedWpPointers(String dismissedWpPointers) {
		Usermeta meta = new Usermeta();
		meta.setDismissedWpPointers(dismissedWpPointers);
		meta.setUserId(getID());
		metas.add(meta);
		return this;
	}

	public Users setSimpleLocalAvatar(String simpleLocalAvatar) {
		Usermeta meta = new Usermeta();
		meta.setSimpleLocalAvatar(simpleLocalAvatar);
		meta.setUserId(getID());
		metas.add(meta);
		return this;
	}
	
	public List<Usermeta> getMetas() {
		return metas;
	}
}
