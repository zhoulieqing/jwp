package com.tbynet.jwp.model;

import com.tbynet.jwp.model.base.BaseLinks;

/**
 * 链接
 * @ClassName: Links
 * @Description: linked to TermRelationships with link_id
 * @Author: 佰亿互联
 * @Date: 2019年3月22日 上午12:21:23
 * 
 * @Copyright: 2019 www.tbynet.com Inc. All rights reserved.
 * 注意: 本内容仅限于内部传阅，禁止外泄以及用于其他的商业目
 */
@SuppressWarnings("serial")
public class Links extends BaseLinks<Links> {
	
}
