package com.tbynet.jwp.model;

import org.phprpc.util.AssocArray;
import org.phprpc.util.Cast;
import org.phprpc.util.PHPSerializer;

import com.jfinal.kit.Kv;
import com.jfinal.kit.StrKit;
import com.tbynet.jwp.model.base.BaseUsermeta;

/**
 * 用户的元数据
 * @ClassName: Usermeta
 * @Description: linked to Users with user_id
 * @Author: 佰亿互联
 * @Date: 2019年3月22日 上午12:23:33
 * 
 * @Copyright: 2019 www.tbynet.com Inc. All rights reserved.
 * 注意: 本内容仅限于内部传阅，禁止外泄以及用于其他的商业目
 */
@SuppressWarnings("serial")
public class Usermeta extends BaseUsermeta<Usermeta> {

	public static final String role = "role"; //角色，自定义，区分能力capabilities
	public static final String permissions = "permissions"; //权限，自定义，区分能力

	public static final String nickname = "nickname"; //Adam
	public static final String first_name	= "first_name";	//
	public static final String last_name = "last_name"; //
	public static final String description = "description"; //
	public static final String rich_editing = "rich_editing"; //true
	public static final String syntax_highlighting = "syntax_highlighting"; //true
	public static final String comment_shortcuts = "comment_shortcuts"; //false
	public static final String admin_color = "admin_color"; //fresh
	public static final String use_ssl = "use_ssl"; //0
	public static final String show_admin_bar_front = "show_admin_bar_front";	//true
	public static final String locale = "locale"; //
	public static final String wp_capabilities = "wp_capabilities"; //能力
	public static final String wp_user_level = "wp_user_level"; //10
	public static final String dismissed_wp_pointers = "dismissed_wp_pointers"; //wp496_privacy,theme_editor_notice
	public static final String show_welcome_panel = "show_welcome_panel"; //1
	public static final String wp_dashboard_quick_press_last_post_id = ""; //6
	public static final String community_events_location = "community-events-location"; //a:1:{s:2:"ip";s:2:"::";}
	public static final String closedpostboxes_page = "closedpostboxes_page"; //a:0:{}
	public static final String metaboxhidden_page = "metaboxhidden_page"; //a:5:{i:0;s:10:"postcustom";i:1;s:16:"commentstatus...
	public static final String wp_user_settings = "wp_user-settings"; //editor=tinymce&mfold=o&libraryContent=browse&hidet...
	public static final String wp_user_settings_time = "wp_user-settings-time"; //1553349434
	public static final String managenav_menuscolumnshidden = "managenav-menuscolumnshidden"; //a:5:{i:0;s:11:"link-target";i:1;s:11:"css-classes"...
	public static final String metaboxhidden_nav_menus = "metaboxhidden_nav-menus"; //a:2:{i:0;s:12:"add-post_tag";i:1;s:15:"add-post_fo...
	public static final String nav_menu_recently_edited = "nav_menu_recently_edited"; //2
	public static final String wp_media_library_mode = "wp_media_library_mode"; //list
	public static final String session_tokens = "session_tokens"; //用户会话

	public static final String simple_local_avatar = "simple_local_avatar"; //用户头像

	public void setFirstName(String value) {
		setMetaKey(first_name);
		setMetaValue(value);
	}
	
	public void setLastName(String value) {
		setMetaKey(last_name);
		setMetaValue(value);
	}
	
	public void setNickname(String value) {
		setMetaKey(nickname);
		setMetaValue(value);
	}
	
	public void setDescription(String value) {
		setMetaKey(description);
		setMetaValue(value);
	}
	
	public void setLocale(String value) {
		setMetaKey(locale);
		setMetaValue(value);
	}
	
	public void setAdminColor(String value) {
		setMetaKey(admin_color);
		setMetaValue(value);
	}
	
	public void setRichEditing(String value) {
		setMetaKey(rich_editing);
		setMetaValue(value);
	}
	
	public void setSyntaxHighlighting(String value) {
		setMetaKey(syntax_highlighting);
		setMetaValue(value);
	}
	
	public void setCommentShortcuts(String value) {
		setMetaKey(comment_shortcuts);
		setMetaValue(value);
	}
	
	public void setShowAdminBarFront(String value) {
		setMetaKey(show_admin_bar_front);
		setMetaValue(value);
	}
	
	public void setUseSSL(String value) {
		setMetaKey(use_ssl);
		setMetaValue(value);
	}
	
	public void setWpUserLevel(String value) {
		setMetaKey(wp_user_level);
		setMetaValue(value);
	}
	
	public void setDismissedWpPointers(String value) {
		setMetaKey(dismissed_wp_pointers);
		setMetaValue(value);
	}
	
	public void setRole(String value) {
		setMetaKey(role);
		setMetaValue(value);
	}

	public void setSimpleLocalAvatar(String value) {
		setMetaKey(simple_local_avatar);
		setMetaValue(value);
	}
	
	public AssocArray getPermissions() throws Exception {
//		数据存储格式
//		a:51:{s:12:"upload_files";a:2:{s:5:"scope";r:3;s:6:"action";s:18:"/admin/file/upload";}}
		
		if (StrKit.isBlank(getMetaValue()))
	        return new AssocArray();
	    
	    PHPSerializer p = new PHPSerializer();
	    
	    return (AssocArray) p.unserialize(getMetaValue().getBytes());
	}
	
	public void setPermissions(Object..._permissions) throws Exception {
		AssocArray array = new AssocArray();
		
		for(Object permission : _permissions) {
			Kv kv = (Kv) permission;
			for(Object key : kv.keySet()) {
				array.set(String.valueOf(key), kv.get(key));
			}
		}
				
		PHPSerializer p = new PHPSerializer();
		
		setMetaKey(permissions);
		setMetaValue(new String(p.serialize(array)));
	}
	
	public void delPermissions(String permission) throws Exception {
		AssocArray array = getWpCapabilities();
		if(null != array && !array.isEmpty()) {
			array.remove(permission);
		}
		
		PHPSerializer p = new PHPSerializer();
		byte[] _permissions = p.serialize(array);
		
		setMetaKey(permissions);
		setMetaValue(new String(_permissions));
	}
	
	public AssocArray getWpCapabilities() throws Exception {
//		数据存储格式
//		a:1:{s:13:"administrator";b:1;}
		
		if (StrKit.isBlank(getMetaValue()))
	        return new AssocArray();
	    
	    PHPSerializer p = new PHPSerializer();
	    
	    return (AssocArray) p.unserialize(getMetaValue().getBytes());
	}
	
	public void setWpCapabilities(Object...capabilities) throws Exception {
		AssocArray array = new AssocArray();
		
		for(Object capability : capabilities) {
			array.set(String.valueOf(capability), true);
		}
				
		PHPSerializer p = new PHPSerializer();
		
		setMetaKey(wp_capabilities);
		setMetaValue(new String(p.serialize(array)));
	}
	
	public void delWpCapabilities(String capability) throws Exception {
		AssocArray array = getWpCapabilities();
		if(null != array && !array.isEmpty()) {
			array.remove(capability);
		}
		
		PHPSerializer p = new PHPSerializer();
		byte[] capabilities = p.serialize(array);
		
		setMetaKey(wp_capabilities);
		setMetaValue(new String(capabilities));
	}
	
	/**
	 * 获取用户会话
	 * @Title: getSessionTokens
	 * @Description: 获取用户所有会话
	 * @param: @return
	 * @param: @throws Exception
	 * @return: AssocArray
	 * @throws
	 */
	public AssocArray getSessionTokens() throws Exception {
//		数据存储格式
//		'a:1:{
//		s:32:"27f32d5d25404c5bb9a1ff5e10771c6c";
//		a:4:{
//			s:2:"ip";
//			s:15:"0:0:0:0:0:0:0:1";
//			s:10:"expiration";
//			d:1648064272013;
//			s:2:"ua";
//			s:119:"mozilla/5.0";
//			s:5:"login";
//			d:1553456272013;}}'
		
	    if (StrKit.isBlank(getMetaValue()))
	        return new AssocArray();
	    
	    PHPSerializer p = new PHPSerializer();
	    
	    return (AssocArray) p.unserialize(getMetaValue().getBytes());
	}
	
	public Kv getSessionTokens(String sessionId) throws Exception {
		AssocArray array = getSessionTokens();
		if(null == array || array.isEmpty()) {
			return null;
		}
		
		Object object = array.toLinkedHashMap().get(sessionId);
		
		return (Kv) Cast.cast(object, Kv.class);
	}
	
	/**
	 * 设置用户会员
	 * @Title: setSessionTokens
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param: @param sessionId 会话id
	 * @param: @param expiration 有效期
	 * @param: @param ip 登录ip
	 * @param: @param ua 代理
	 * @param: @param login 登录时间
	 * @param: @throws Exception
	 * @return: void
	 * @throws
	 */
	public void setSessionTokens(String sessionId, long expiration, String ip, String ua, long login) throws Exception {
		Kv session = Kv.by("expiration", expiration).set("ip", ip).set("ua", ua).set("login", login);
		boolean exists = false;
		
		AssocArray array = getSessionTokens();
		if(null != array && !array.isEmpty()) {
			if(null != array.get(sessionId)) {
				array.set(sessionId, session);
				exists = true;
			}
		}
		
		if(false == exists) {
			array.set(sessionId, session);
		}
				
		PHPSerializer p = new PHPSerializer();
		byte[] sessions = p.serialize(array);
		
		setMetaKey(session_tokens);
		setMetaValue(new String(sessions));
	}
	
	/**
	 * 删除用户会话
	 * @Title: delSessionTokens
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param: @param sessionId 会话id
	 * @param: @throws Exception
	 * @return: void
	 * @throws
	 */
	public void delSessionTokens(String sessionId) throws Exception {
		AssocArray array = getSessionTokens();
		if(null != array && !array.isEmpty()) {
			array.remove(sessionId);
		}
				
		PHPSerializer p = new PHPSerializer();
		byte[] sessions = p.serialize(array);
		
		setMetaKey(session_tokens);
		setMetaValue(new String(sessions));
	}
	
}
