package com.tbynet.jwp.model;

import com.tbynet.jwp.model.base.BaseTermmeta;

/**
 * 分项项的元数据
 * @ClassName: Termmeta
 * @Description: linked to Terms with term_id
 * @Author: 佰亿互联
 * @Date: 2019年3月22日 上午12:25:32
 * 
 * @Copyright: 2019 www.tbynet.com Inc. All rights reserved.
 * 注意: 本内容仅限于内部传阅，禁止外泄以及用于其他的商业目
 */
@SuppressWarnings("serial")
public class Termmeta extends BaseTermmeta<Termmeta> {
	
}
