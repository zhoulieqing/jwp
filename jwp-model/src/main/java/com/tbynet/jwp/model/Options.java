package com.tbynet.jwp.model;

import org.phprpc.util.AssocArray;
import org.phprpc.util.PHPSerializer;

import com.jfinal.kit.StrKit;
import com.tbynet.jwp.model.base.BaseOptions;

/**
 * 设置选项
 * @ClassName: Options
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @Author: 佰亿互联
 * @Date: 2019年3月22日 上午12:22:22
 * 
 * @Copyright: 2019 www.tbynet.com Inc. All rights reserved.
 * 注意: 本内容仅限于内部传阅，禁止外泄以及用于其他的商业目
 */
@SuppressWarnings("serial")
public class Options extends BaseOptions<Options> {

	public static final String siteurl = "siteurl"; //http://localhost:8888	yes	
	public static final String home = "home"; //http://localhost:8888	yes	
	public static final String blogname = "blogname"; //测试	yes	
	public static final String blogdescription = "blogdescription"; //又一个WordPress站点	yes	
	public static final String users_can_register = "users_can_register"; //0	yes	
	public static final String admin_email = "admin_email"; //adam@tbynet.com	yes	
	public static final String start_of_week = "start_of_week"; //1	yes	
	public static final String use_balanceTags = "use_balanceTags"; //0	yes	
	public static final String use_smilies = "use_smilies"; //1	yes	
	public static final String require_name_email = "require_name_email"; //1	yes	
	public static final String comments_notify = "comments_notify"; //1	yes	
	public static final String posts_per_rss = "posts_per_rss"; //10	yes	
	public static final String rss_use_excerpt = "rss_use_excerpt"; //0	yes	
	public static final String mailserver_url = "mailserver_url"; //mail.example.com	yes	
	public static final String mailserver_login = "mailserver_login"; //login@example.com	yes	
	public static final String mailserver_pass = "mailserver_pass"; //password	yes	
	public static final String mailserver_port = "mailserver_port"; //110	yes	
	public static final String default_category = "default_category"; //1	yes	
	public static final String default_comment_status = "default_comment_status"; //open	yes	
	public static final String default_ping_status = "default_ping_status"; //open	yes	
	public static final String default_pingback_flag = "default_pingback_flag"; //1	yes	
	public static final String posts_per_page = "posts_per_page"; //10	yes	
	public static final String date_format = "date_format"; //Y年n月j日	yes	
	public static final String time_format = "time_format"; //ag:i	yes	
	public static final String links_updated_date_format = "links_updated_date_format"; //Y年n月j日ag:i	yes	
	public static final String comment_moderation = "comment_moderation"; //0	yes	
	public static final String moderation_notify = "moderation_notify"; //1	yes	
	public static final String permalink_structure = "permalink_structure"; ///%year%/%monthnum%/%day%/%postname%/	yes	
	public static final String rewrite_rules = "rewrite_rules"; //a:90:{s:11:"^wp-json/?$";s:22:"index.php?rest_rout...	yes	
	public static final String hack_file = "hack_file"; //0	yes	
	public static final String blog_charset = "blog_charset"; //UTF-8	yes	
	public static final String moderation_keys = "moderation_keys"; //no	
	public static final String active_plugins = "active_plugins"; //a:0:{}	yes	
	public static final String category_base = "category_base"; //yes	
	public static final String ping_sites = "ping_sites"; //http://rpc.pingomatic.com/	yes	
	public static final String comment_max_links = "comment_max_links"; //2	yes	
	public static final String gmt_offset = "gmt_offset"; //0	yes	
	public static final String default_email_category = "default_email_category"; //1	yes	
	public static final String recently_edited = "recently_edited"; //a:5:{i:0;s:76:"/Users/Adam/Documents/wordpress/wp-...	no	
	public static final String template = "template"; //twentyseventeen	yes	
	public static final String stylesheet = "stylesheet"; //twentyseventeen	yes	
	public static final String comment_whitelist = "comment_whitelist"; //1	yes	
	public static final String blacklist_keys = "blacklist_keys"; //no	
	public static final String comment_registration = "comment_registration"; //0	yes	
	public static final String html_type = "html_type"; //text/html	yes	
	public static final String use_trackback = "use_trackback"; //0	yes	
	public static final String default_role = "default_role"; //subscriber	yes	
	public static final String db_version = "db_version"; //38590	yes	
	public static final String uploads_use_yearmonth_folders = "uploads_use_yearmonth_folders"; //1	yes	
	public static final String upload_path = "upload_path"; //yes	
	public static final String blog_public = "blog_public"; //1	yes	
	public static final String default_link_category = "default_link_category"; //2	yes	
	public static final String show_on_front = "show_on_front"; //posts	yes	
	public static final String tag_base = "tag_base"; //yes	
	public static final String show_avatars = "show_avatars"; //1	yes	
	public static final String avatar_rating = "avatar_rating"; //G	yes	
	public static final String upload_url_path = "upload_url_path"; //yes	
	public static final String thumbnail_size_w = "thumbnail_size_w"; //150	yes	
	public static final String thumbnail_size_h = "thumbnail_size_h"; //150	yes	
	public static final String thumbnail_crop = "thumbnail_crop"; //1	yes	
	public static final String medium_size_w = "medium_size_w"; //300	yes	
	public static final String medium_size_h = "medium_size_h"; //300	yes	
	public static final String avatar_default = "avatar_default"; //mystery	yes	
	public static final String large_size_w = "large_size_w"; //1024	yes	
	public static final String large_size_h = "large_size_h"; //1024	yes	
	public static final String image_default_link_type = "image_default_link_type"; //none	yes	
	public static final String image_default_size = "image_default_size"; //yes	
	public static final String image_default_align = "image_default_align"; //yes	
	public static final String close_comments_for_old_posts = "close_comments_for_old_posts"; //0	yes	
	public static final String close_comments_days_old = "close_comments_days_old"; //14	yes	
	public static final String thread_comments = "thread_comments"; //1	yes	
	public static final String thread_comments_depth = "thread_comments_depth"; //5	yes	
	public static final String page_comments = "page_comments"; //0	yes	
	public static final String comments_per_page = "comments_per_page"; //50	yes	
	public static final String default_comments_page = "default_comments_page"; //newest	yes	
	public static final String comment_order = "comment_order"; //asc	yes	
	public static final String sticky_posts = "sticky_posts"; //a:0:{}	yes	
	public static final String widget_categories = "widget_categories"; //a:2:{i:2;a:4:{s:5:"title";s:0:"";s:5:"count";i:0;s...	yes	
	public static final String widget_text = "widget_text"; //a:5:{i:2;a:4:{s:5:"title";s:12:"联系我们";s:4:"text";s...	yes	
	public static final String widget_rss = "widget_rss"; //a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}	yes	
	public static final String uninstall_plugins = "uninstall_plugins"; //a:0:{}	no	
	public static final String timezone_string = "timezone_string"; //Asia/Shanghai	yes	
	public static final String page_for_posts = "page_for_posts"; //0	yes	
	public static final String page_on_front = "page_on_front"; //0	yes	
	public static final String default_post_format = "default_post_format"; //0	yes	
	public static final String link_manager_enabled = "link_manager_enabled"; //0	yes	
	public static final String finished_splitting_shared_terms = "finished_splitting_shared_terms"; //1	yes	
	public static final String site_icon = "site_icon"; //0	yes	
	public static final String medium_large_size_w = "medium_large_size_w"; //768	yes	
	public static final String medium_large_size_h = "medium_large_size_h"; //0	yes	
	public static final String initial_db_version = "initial_db_version"; //38590	yes	
	public static final String wp_user_roles = "wp_user_roles"; //a:5:{s:13:"administrator";a:2:{s:4:"name";s:13:"Ad...	yes	
	public static final String fresh_site = "fresh_site"; //1	yes	
	public static final String WPLANG = "WPLANG"; //zh_CN	yes	
	public static final String widget_search = "widget_search"; //a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidg...	yes	
	public static final String widget_recent_posts = "widget_recent-posts"; //a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;...	yes	
	public static final String widget_recent_comments = "widget_recent-comments"; //a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;...	yes	
	public static final String widget_archives = "widget_archives"; //a:2:{i:2;a:3:{s:5:"title";s:0:"";s:5:"count";i:0;s...	yes	
	public static final String widget_meta = "widget_meta"; //a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidg...	yes	
	public static final String sidebars_widgets = "sidebars_widgets"; //a:5:{s:19:"wp_inactive_widgets";a:0:{}s:9:"sidebar...	yes	
	public static final String widget_pages = "widget_pages"; //a:1:{s:12:"_multiwidget";i:1;}	yes	
	public static final String widget_calendar = "widget_calendar"; //a:1:{s:12:"_multiwidget";i:1;}	yes	
	public static final String widget_media_audio = "widget_media_audio"; //a:1:{s:12:"_multiwidget";i:1;}	yes	
	public static final String widget_media_image = "widget_media_image"; //a:1:{s:12:"_multiwidget";i:1;}	yes	
	public static final String widget_media_gallery = "widget_media_gallery"; //a:1:{s:12:"_multiwidget";i:1;}	yes	
	public static final String widget_media_video = "widget_media_video"; //a:1:{s:12:"_multiwidget";i:1;}	yes	
	public static final String widget_tag_cloud = "widget_tag_cloud"; //a:1:{s:12:"_multiwidget";i:1;}	yes	
	public static final String widget_nav_menu = "widget_nav_menu"; //a:1:{s:12:"_multiwidget";i:1;}	yes	
	public static final String widget_custom_html = "widget_custom_html"; //a:1:{s:12:"_multiwidget";i:1;}	yes	
	public static final String cron = "cron"; //a:5:{i:1553480472;a:3:{s:16:"wp_version_check";a:1...	yes	
	public static final String can_compress_scripts = "can_compress_scripts"; //1	no	
	public static final String recently_activated = "recently_activated"; //a:0:{}	yes	
	public static final String category_children = "category_children"; //a:0:{}	yes	
	public static final String auto_core_update_notified = "auto_core_update_notified"; //a:4:{s:4:"type";s:7:"success";s:5:"email";s:15:"ad...	no	
	public static final String nav_menu_options = "nav_menu_options"; //a:2:{i:0;b:0;s:8:"auto_add";a:0:{}}	yes	
	public static final String show_comments_cookies_opt_in = "show_comments_cookies_opt_in";
	public static final String new_admin_email = "new_admin_email";
	public static final String zh_cn_l10n_icp_num = "zh_cn_l10n_icp_num";
	public static final String wp_page_for_privacy_policy = "wp_page_for_privacy_policy";
	
	//WordPress地址
	public void setSiteUrl(String value) {
		setOptionName(siteurl);
		setOptionValue(value);
	}
	
	//站点地址
	public void setHome(String value) {
		setOptionName(home);
		setOptionValue(value);
	}
	
	//站点标题
	public void setBlogName(String value) {
		setOptionName(blogname);
		setOptionValue(value);
	}
	
	//副标题
	public void setBlogDescription(String value) {
		setOptionName(blogdescription);
		setOptionValue(value);
	}
	
	//成员资格
	public void setUsersCanRegister(String value) {
		setOptionName(users_can_register);
		setOptionValue(value);
	}
	
	//电子邮件地址
	public void setAdminEmail(String value) {
		setOptionName(admin_email);
		setOptionValue(value);
	}
	
	//一星期开始于
	public void setStartOfWeek(String value) {
		setOptionName(start_of_week);
		setOptionValue(value);
	}
	
	public void setUseBalanceTags(String value) {
		setOptionName(use_balanceTags);
		setOptionValue(value);
	}
	
	public void setUseSmilies(String value) {
		setOptionName(use_smilies);
		setOptionValue(value);
	}
	
	public void setRequireNameEmail(String value) {
		setOptionName(require_name_email);
		setOptionValue(value);
	}
	
	public void setCommentsNotify(String value) {
		setOptionName(comments_notify);
		setOptionValue(value);
	}
	
	public void setPostsPerRSS(String value) {
		setOptionName(posts_per_rss);
		setOptionValue(value);
	}
	
	public void setRSSUseExcerpt(String value) {
		setOptionName(rss_use_excerpt);
		setOptionValue(value);
	}
	
	public void setMailServerUrl(String value) {
		setOptionName(mailserver_url);
		setOptionValue(value);
	}
	
	public void setMailServerLogin(String value) {
		setOptionName(mailserver_login);
		setOptionValue(value);
	}
	
	public void setMailServerPass(String value) {
		setOptionName(mailserver_pass);
		setOptionValue(value);
	}
	
	public void setMailServerPort(String value) {
		setOptionName(mailserver_port);
		setOptionValue(value);
	}
	
	public void setDefaultCategory(String value) {
		setOptionName(default_category);
		setOptionValue(value);
	}
	
	public void setDefaultCommentStatus(String value) {
		setOptionName(default_comment_status);
		setOptionValue(value);
	}
	
	public void setDefaultPingStatus(String value) {
		setOptionName(default_ping_status);
		setOptionValue(value);
	}
	
	public void setDefaultPingbackFlag(String value) {
		setOptionName(default_pingback_flag);
		setOptionValue(value);
	}
	
	public void setPostsPerPage(String value) {
		setOptionName(posts_per_page);
		setOptionValue(value);
	}
	
	//日期格式
	public void setDateFormat(String value) {
		setOptionName(date_format);
		setOptionValue(value);
	}
	
	//时间格式
	public void setTimeFormat(String value) {
		setOptionName(time_format);
		setOptionValue(value);
	}
	
	public void setLinksUpdateDateFormat(String value) {
		setOptionName(links_updated_date_format);
		setOptionValue(value);
	}
	
	public void setCommentModeration(String value) {
		setOptionName(comment_moderation);
		setOptionValue(value);
	}
	
	public void setModerationNotify(String value) {
		setOptionName(moderation_notify);
		setOptionValue(value);
	}
	
	public void setPermalinkStructure(String value) {
		setOptionName(permalink_structure);
		setOptionValue(value);
	}
	
	public void setRewriteRules(String value) {
		setOptionName(rewrite_rules);
		setOptionValue(value);
	}
	
	public void setHackFile(String value) {
		setOptionName(hack_file);
		setOptionValue(value);
	}
	
	public void setBlogCharset(String value) {
		setOptionName(blog_charset);
		setOptionValue(value);
	}
	
	public void setModerationKeys(String value) {
		setOptionName(moderation_keys);
		setOptionValue(value);
	}
	
	public void setActivePlugins(String value) {
		setOptionName(active_plugins);
		setOptionValue(value);
	}
	
	public void setCategoryBase(String value) {
		setOptionName(category_base);
		setOptionValue(value);
	}
	
	public void setPingSites(String value) {
		setOptionName(ping_sites);
		setOptionValue(value);
	}
	
	public void setCommentMaxLinks(String value) {
		setOptionName(comment_max_links);
		setOptionValue(value);
	}
	
	public void setGmtOffset(String value) {
		setOptionName(gmt_offset);
		setOptionValue(value);
	}
	
	public void setDefaultEmailCategory(String value) {
		setOptionName(default_email_category);
		setOptionValue(value);
	}
	
	public void setRecentlyEdited(String value) {
		setOptionName(recently_edited);
		setOptionValue(value);
	}
	
	public void setTemplate(String value) {
		setOptionName(template);
		setOptionValue(value);
	}
	
	public void setStylesheet(String value) {
		setOptionName(stylesheet);
		setOptionValue(value);
	}
	
	public void setCommentWhitelist(String value) {
		setOptionName(comment_whitelist);
		setOptionValue(value);
	}
	
	public void setBlacklistKeys(String value) {
		setOptionName(blacklist_keys);
		setOptionValue(value);
	}
	
	public void setCommentRegistration(String value) {
		setOptionName(comment_registration);
		setOptionValue(value);
	}
	
	public void setHtmlType(String value) {
		setOptionName(html_type);
		setOptionValue(value);
	}
	
	public void setUseTrackback(String value) {
		setOptionName(use_trackback);
		setOptionValue(value);
	}
	
	//新用户默认角色
	public void setDefaultRole(String value) {
		setOptionName(default_role);
		setOptionValue(value);
	}
	
	public void setDbVersion(String value) {
		setOptionName(db_version);
		setOptionValue(value);
	}
	
	public void setUploadsUseYearmonthFolder(String value) {
		setOptionName(uploads_use_yearmonth_folders);
		setOptionValue(value);
	}
	
	public void setUploadPath(String value) {
		setOptionName(upload_path);
		setOptionValue(value);
	}
	
	public void setBlogPublic(String value) {
		setOptionName(blog_public);
		setOptionValue(value);
	}
	
	public void setDefaultLinkCategory(String value) {
		setOptionName(default_link_category);
		setOptionValue(value);
	}
	
	public void setShowOnFront(String value) {
		setOptionName(show_on_front);
		setOptionValue(value);
	}
	
	public void setTagBase(String value) {
		setOptionName(tag_base);
		setOptionValue(value);
	}
	
	public void setShowAvatars(String value) {
		setOptionName(show_avatars);
		setOptionValue(value);
	}
	
	public void setAvatarRating(String value) {
		setOptionName(avatar_rating);
		setOptionValue(value);
	}
	
	public void setUploadUrlPath(String value) {
		setOptionName(upload_url_path);
		setOptionValue(value);
	}
	
	public void setThumbnailSizeW(String value) {
		setOptionName(thumbnail_size_w);
		setOptionValue(value);
	}
	
	public void setThumbnailSizeH(String value) {
		setOptionName(thumbnail_size_h);
		setOptionValue(value);
	}
	
	public void setThumbnailCrop(String value) {
		setOptionName(thumbnail_crop);
		setOptionValue(value);
	}
	
	public void setMediumSizeW(String value) {
		setOptionName(medium_size_w);
		setOptionValue(value);
	}
	
	public void setMediumSizeH(String value) {
		setOptionName(medium_size_h);
		setOptionValue(value);
	}
	
	public void setAvatarDefault(String value) {
		setOptionName(avatar_default);
		setOptionValue(value);
	}
	
	public void setLargeSizeW(String value) {
		setOptionName(large_size_w);
		setOptionValue(value);
	}
	
	public void setLargeSizeH(String value) {
		setOptionName(large_size_h);
		setOptionValue(value);
	}
	
	public void setImageDefaultLinkType(String value) {
		setOptionName(image_default_link_type);
		setOptionValue(value);
	}
	
	public void setImageDefaultSize(String value) {
		setOptionName(image_default_size);
		setOptionValue(value);
	}
	
	public void setImageDefaultAlign(String value) {
		setOptionName(image_default_align);
		setOptionValue(value);
	}
	
	public void setCloseCommentsForOldPosts(String value) {
		setOptionName(close_comments_for_old_posts);
		setOptionValue(value);
	}
	
	public void setCloseCommentsDaysOld(String value) {
		setOptionName(close_comments_days_old);
		setOptionValue(value);
	}
	
	public void setThreadComments(String value) {
		setOptionName(thread_comments);
		setOptionValue(value);
	}
	
	public void setThreadCommentsDepth(String value) {
		setOptionName(thread_comments_depth);
		setOptionValue(value);
	}
	
	public void setPageComments(String value) {
		setOptionName(page_comments);
		setOptionValue(value);
	}
	
	public void setCommentsPerPage(String value) {
		setOptionName(comments_per_page);
		setOptionValue(value);
	}
	
	public void setDefaultCommentspage(String value) {
		setOptionName(default_comments_page);
		setOptionValue(value);
	}
	
	public void setCommentOrder(String value) {
		setOptionName(comment_order);
		setOptionValue(value);
	}
	
	//设置置顶文章
	public void setStickyPosts(Integer value) throws Exception {
		AssocArray array = getStickyPosts();
		if(null != array) {
			int index = array.toArrayList().indexOf(value);
			if(-1 == index) {
				array.add(value);
			} else {
				array.set(index, value);
			}
		}
				
		PHPSerializer p = new PHPSerializer();
		
		setOptionName(sticky_posts);
		setOptionValue(new String(p.serialize(array)));
	}
	
	public AssocArray getStickyPosts() throws Exception {
//		数据库存储格式
//		a:1:{i:0;i:71;}
		
		if (StrKit.isBlank(getOptionValue()))
	        return new AssocArray();
	    
	    PHPSerializer p = new PHPSerializer();
	    
	    return (AssocArray) p.unserialize(getOptionValue().getBytes());
	}
	
	public boolean hasStickyPosts(Integer value) throws Exception {
		AssocArray array = getStickyPosts();
		if(null != array) {
			return array.toArrayList().contains(value);
		}
		
		return false;
	}
	
	public void setWidgetCategories(String value) {
		setOptionName(widget_categories);
		setOptionValue(value);
	}
	
	public void setWidgetText(String value) {
		setOptionName(widget_text);
		setOptionValue(value);
	}
	
	public void setWidgetRSS(String value) {
		setOptionName(widget_rss);
		setOptionValue(value);
	}
	
	public void setUninstallPlugins(String value) {
		setOptionName(uninstall_plugins);
		setOptionValue(value);
	}
	
	//时区
	public void setTimezoneString(String value) {
		setOptionName(timezone_string);
		setOptionValue(value);
	}
	
	public void setPageForPosts(String value) {
		setOptionName(page_for_posts);
		setOptionValue(value);
	}
	
	public void setPageOnFront(String value) {
		setOptionName(page_on_front);
		setOptionValue(value);
	}
	
	public void setDefaultPostFormat(String value) {
		setOptionName(default_post_format);
		setOptionValue(value);
	}
	
	public void setLinkManagerEnabled(String value) {
		setOptionName(link_manager_enabled);
		setOptionValue(value);
	}
	
	public void setFinishedSplittingSharedTerms(String value) {
		setOptionName(finished_splitting_shared_terms);
		setOptionValue(value);
	}
	
	public void setSiteIcon(String value) {
		setOptionName(site_icon);
		setOptionValue(value);
	}
	
	public void setMediumLargeSizeW(String value) {
		setOptionName(medium_large_size_w);
		setOptionValue(value);
	}
	
	public void setMediumLargeSizeH(String value) {
		setOptionName(medium_large_size_h);
		setOptionValue(value);
	}
	
	public void setInitialDbVersion(String value) {
		setOptionName(initial_db_version);
		setOptionValue(value);
	}
	
	public void setWpUserRoles(String value) {
		setOptionName(wp_user_roles);
		setOptionValue(value);
	}
	
	public void setFreshSite(String value) {
		setOptionName(fresh_site);
		setOptionValue(value);
	}
	
	//站点语言
	public void setWpLang(String value) {
		setOptionName(WPLANG);
		setOptionValue(value);
	}
	
	public void setWidgetSearch(String value) {
		setOptionName(widget_search);
		setOptionValue(value);
	}
	
	public void setWidgetRecentPosts(String value) {
		setOptionName(widget_recent_posts);
		setOptionValue(value);
	}
	
	public void setWidgetRecentComments(String value) {
		setOptionName(widget_recent_comments);
		setOptionValue(value);
	}
	
	public void setWidgetArchives(String value) {
		setOptionName(widget_archives);
		setOptionValue(value);
	}
	
	public void setWidgetMeta(String value) {
		setOptionName(widget_meta);
		setOptionValue(value);
	}
	
	public void setSidebarsWidgets(String value) {
		setOptionName(sidebars_widgets);
		setOptionValue(value);
	}
	
	public void setWidgetPages(String value) {
		setOptionName(widget_pages);
		setOptionValue(value);
	}
	
	public void setWidgetCalendar(String value) {
		setOptionName(widget_calendar);
		setOptionValue(value);
	}
	
	public void setWidgetMediaAudio(String value) {
		setOptionName(widget_media_audio);
		setOptionValue(value);
	}
	
	public void setWidgetMediaImage(String value) {
		setOptionName(widget_media_image);
		setOptionValue(value);
	}
	
	public void setWidgetMediaVideo(String value) {
		setOptionName(widget_media_video);
		setOptionValue(value);
	}
	
	public void setWidgetTagCloud(String value) {
		setOptionName(widget_tag_cloud);
		setOptionValue(value);
	}
	
	public void setWidgetNavMenu(String value) {
		setOptionName(widget_nav_menu);
		setOptionValue(value);
	}
	
	public void setWidgetCustomHtml(String value) {
		setOptionName(widget_custom_html);
		setOptionValue(value);
	}
	
	public void setCron(String value) {
		setOptionName(cron);
		setOptionValue(value);
	}
	
	public void setCanCompressScripts(String value) {
		setOptionName(can_compress_scripts);
		setOptionValue(value);
	}
	
	public void setRecentlyActivated(String value) {
		setOptionName(recently_activated);
		setOptionValue(value);
	}
	
	public void setCategoryChidren(String value) {
		setOptionName(category_children);
		setOptionValue(value);
	}
	
	public void setAutoCoreUpdateNotified(String value) {
		setOptionName(auto_core_update_notified);
		setOptionValue(value);
	}
	
	public void setNavMenuOptions(String value) {
		setOptionName(nav_menu_options);
		setOptionValue(value);
	}
	
	public void setShowCommentsCookiesOptIn(String value) {
		setOptionName(show_comments_cookies_opt_in);
		setOptionValue(value);
	}
	
	//电子邮件地址(新)
	public void setNewAdminEmail(String value) {
		setOptionName(new_admin_email);
		setOptionValue(value);
	}
	
	//ICP备案号
	public void setZhCnL10nIcpNum(String value) {
		setOptionName(zh_cn_l10n_icp_num);
		setOptionValue(value);
	}
	
	//隐私政策页面
	public void setWpPageForPrivacyPolicy(String value) {
		setOptionName(wp_page_for_privacy_policy);
		setOptionValue(value);
	}
	
}
