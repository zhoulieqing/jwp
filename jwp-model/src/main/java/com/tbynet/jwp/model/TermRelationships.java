package com.tbynet.jwp.model;

import com.tbynet.jwp.model.base.BaseTermRelationships;

/**
 * 文章与分类法的关系
 * @ClassName: TermRelationships
 * @Description: linked to Posts with post_id, TermTaxonomy with term_taxonomy_id
 * @Author: 佰亿互联
 * @Date: 2019年3月22日 上午12:24:10
 * 
 * @Copyright: 2019 www.tbynet.com Inc. All rights reserved.
 * 注意: 本内容仅限于内部传阅，禁止外泄以及用于其他的商业目
 */
@SuppressWarnings("serial")
public class TermRelationships extends BaseTermRelationships<TermRelationships> {
	
}
