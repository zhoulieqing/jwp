package com.tbynet.jwp.model;

import javax.sql.DataSource;

import com.jfinal.plugin.activerecord.dialect.MysqlDialect;
import com.jfinal.plugin.activerecord.generator.Generator;
import com.jfinal.plugin.druid.DruidPlugin;

public class _ModelKit {
	
	private static final String url = "jdbc:mysql://localhost:3306/wordpress?characterEncoding=UTF-8&zeroDateTimeBehavior=convertToNull";
	
	public static DataSource getDataSource() {
		DruidPlugin druidPlugin = new DruidPlugin(url, "root", "root");
		druidPlugin.start();		
		return druidPlugin.getDataSource();
	}
	
	public static void main(String[] args) {
		// base model 所使用的包名
		String baseModelPackageName = "com.tbynet.jwp.model.base";
		// base model 文件保存路径
		String baseModelOutputDir = System.getProperty("user.dir") + "/src/main/java/com/tbynet/jwp/model/base";
		// model 所使用的包名
		String modelPackageName = "com.tbynet.jwp.model";
		// model 文件保存路径
		String modelOutputDir = System.getProperty("user.dir") + "/src/main/java/com/tbynet/jwp/model";
		
		// 创建生成器
		Generator generator = new Generator(getDataSource(), baseModelPackageName, baseModelOutputDir, modelPackageName, modelOutputDir);
		// 设置数据库方言
		generator.setDialect(new MysqlDialect());
		// 添加不需要生成的表名
//		generator.addExcludedTable("t_security_permission");
		// 设置是否在 Model 中生成 dao 对象
		generator.setGenerateDaoInModel(false);
		// 设置是否生成字典文件
//		gernerator.setGenerateDataDictionary(true);
		// 设置需要被移除的表名前缀用于生成modelName。例如表名 "osc_user"，移除前缀 "osc_"后生成的model名为 "User"而非 OscUser
		generator.setRemovedTableNamePrefixes("wp_");
		// 生成
		generator.generate();
	}
}
