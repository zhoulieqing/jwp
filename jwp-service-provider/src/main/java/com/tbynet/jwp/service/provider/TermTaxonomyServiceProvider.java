package com.tbynet.jwp.service.provider;

import com.tbynet.jwp.model.TermTaxonomy;
import com.tbynet.jwp.service.TermTaxonomyService;

public class TermTaxonomyServiceProvider implements TermTaxonomyService {

	private TermTaxonomy taxonomyDao = new TermTaxonomy().dao();
	
	@Override
	public TermTaxonomy getTermTaxonomy(Object termTaxonomyId) {
		return taxonomyDao.findById(termTaxonomyId);
	}

	@Override
	public TermTaxonomy getTermTaxonomy(Object termId, String taxonomy) {
		return taxonomyDao.findFirst("SELECT * FROM wp_term_taxonomy WHERE term_id=? AND taxonomy=? LIMIT 1", termId, taxonomy);
	}

}
