package com.tbynet.jwp.service.provider;

import com.jfinal.aop.AopManager;
import com.tbynet.jwp.service.CommentService;
import com.tbynet.jwp.service.OptionService;
import com.tbynet.jwp.service.PostService;
import com.tbynet.jwp.service.TermService;
import com.tbynet.jwp.service.TermTaxonomyService;
import com.tbynet.jwp.service.UserService;

public class _MappingKit {

	public static void mapping(AopManager me) {
		me.addMapping(UserService.class, UserServiceProvider.class);
		me.addMapping(OptionService.class, OptionServiceProvider.class);
		me.addMapping(PostService.class, PostServiceProvider.class);
		me.addMapping(TermService.class, TermServiceProvider.class);
		me.addMapping(TermTaxonomyService.class, TermTaxonomyServiceProvider.class);
		me.addMapping(CommentService.class, CommentServiceProvider.class);
	}
}
