package com.tbynet.jwp.service;

import java.util.List;
import java.util.Map;

import com.jfinal.plugin.activerecord.Page;
import com.tbynet.jwp.model.TermTaxonomy;
import com.tbynet.jwp.model.Terms;

public interface TermService {

	public Terms getTerm(Object termId);
	
	public Terms getTerm(Object termId, String taxonomy);

	public Terms getTerm(Object objectId, Object termTaxonomyId, String taxonomy);
	
	public List<Terms> getTerms(String taxonomy);
	
	public List<Terms> getTerms(String taxonomy, String q);
	
	public List<Terms> getTerms(Object objectId, String taxonomy);
	
	public Map<String, List<Terms>> getParents(String taxonomy);
	
	public Page<Terms> search(int pageNumber, int pageSize, String taxonomy, String q);
	
	public boolean save(Terms term, TermTaxonomy taxonomy);
	
	public boolean update(Terms term, TermTaxonomy taxonomy);
	
	public boolean delete(Object termId, String taxonomy);
}
