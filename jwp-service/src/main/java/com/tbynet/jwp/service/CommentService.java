package com.tbynet.jwp.service;

import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

import java.util.List;

public interface CommentService {

	public int count(Object postId);

	public List<Record> get_pending_comments_num(String[] postIds);
	public Long get_comment_ids(String status);
	public Page<Record> get_comment_ids(String status, int pageNumber, int pageSize);
	public List<Record> get_comment_count();

}
