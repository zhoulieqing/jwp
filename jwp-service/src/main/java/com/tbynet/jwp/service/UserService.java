package com.tbynet.jwp.service;

import java.util.List;

import com.jfinal.kit.Ret;
import com.jfinal.plugin.activerecord.Page;
import com.tbynet.jwp.model.Usermeta;
import com.tbynet.jwp.model.Users;

public interface UserService {
	
	/**
	 * 登录
	 * @Title: login
	 * @Description: 登录成功返回 sessionId 与 loginUser，否则返回一个 fail msg
	 * @param: @param username 用户名或电子邮箱
	 * @param: @param password 密码
	 * @param: @param keepLogin 是否保持登录
	 * @param: @param loginIp 登录ip
	 * @param: @param userAgent 浏览器代理
	 * @param: @return
	 * @return: Ret
	 * @throws
	 */
	public Ret login(String username, String password, boolean keepLogin, String loginIp, String userAgent);
	
	/**
	 * 获取登录用户
	 * @Title: getLoginUserBySessionId
	 * @Description: 根据当前会话id返回用户
	 * @param: @param sessionId
	 * @param: @return
	 * @return: Users
	 * @throws
	 */
	public Users getLoginUserBySessionId(String sessionId);
	
	/**
	 * 退出登录
	 * @Title: logout
	 * @Description: 当前登录用户退出
	 * @param: @param sessionId 当前会话id
	 * @return: void
	 * @throws
	 */
	public void logout(String sessionId);
	
	/**
	 * 登出其他设备
	 * @Title: destroy
	 * @Description: 除掉当前设备，其它设备都登出
	 * @param: @param sessionId 当前会话id
	 * @param: @return
	 * @return: boolean
	 * @throws
	 */
	public boolean destroy(String sessionId);
	
	/**
	 * 获取用户
	 * @Title: getUser
	 * @Description: 根据用户id返回用户
	 * @param: @param ID 用户id
	 * @param: @return
	 * @return: Users
	 * @throws
	 */
	public Users getUser(Object ID);
	
	/**
	 * 获取用户元数据集合
	 * @Title: getUsermetas
	 * @Description: 根据用户id返回所有用户元数据集合
	 * @param: @param ID 用户id
	 * @param: @return
	 * @return: List<Usermeta>
	 * @throws
	 */
	public List<Usermeta> getUsermetas(Object ID);
	
	/**
	 * 获取用户元数据集合
	 * @Title: getUsermetas
	 * @Description: 根据用户id和元数据key数组返回用户元数据集合
	 * @param: @param ID
	 * @param: @param metaKeys
	 * @param: @return
	 * @return: List<Usermeta>
	 * @throws
	 */
	public List<Usermeta> getUsermetas(Object ID, String...metaKeys);
	
	/**
	 * 获取指定用户元数据
	 * @Title: getUsermeta
	 * @Description: 根据用户id和元数据key返回用户元数据
	 * @param: @param ID 用户id
	 * @param: @param metaKey 元数据key
	 * @param: @return
	 * @return: Usermeta
	 * @throws
	 */
	public Usermeta getUsermeta(Object ID, String metaKey);
	
	/**
	 * 更新用户信息
	 * @Title: update
	 * @Description: 更新用户信息，包括用户元数据
	 * @param: @param user 用户信息
	 * @param: @param metas 用户元数据集合
	 * @param: @return
	 * @return: boolean
	 * @throws
	 */
	public boolean update(Users user, List<Usermeta> metas);
	
	public boolean save(Users user, List<Usermeta> metas);
	
	public boolean save(Users user);
	
	public Page<Users> search(int pageNumber, int pageSize, String q);
}
