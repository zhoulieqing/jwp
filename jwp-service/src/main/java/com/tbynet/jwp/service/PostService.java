package com.tbynet.jwp.service;

import java.util.List;

import com.jfinal.plugin.activerecord.Page;
import com.tbynet.jwp.model.Postmeta;
import com.tbynet.jwp.model.Posts;

public interface PostService {
	
	public Page<Posts> search(int pageNumber, int pageSize, String type, String status, String q);
	
	public Posts getPost(Object ID);
	
	public Posts getPost(Object ID, String type);
	
	public int count(String status);
	
	public List<Postmeta> getPostmetas(Object ID);
	
	public List<Postmeta> getPostmetas(Object ID, String...metaKeys);
	
	public Postmeta getPostmeta(Object ID, String metaKey);
	
	public boolean save(Posts post, List<Postmeta> postmetas, String format, String[] categories, String[] tags, Boolean sticky);
	
	public boolean update(Posts post, List<Postmeta> postmetas, String format, String[] categories, String[] tags, Boolean sticky);
	
	public boolean save(Posts post, List<Postmeta> postmetas);
	
	public boolean update(Posts post, List<Postmeta> postmetas);
			
	public boolean save(Posts post);
	
	public boolean update(Posts post);
	
}
