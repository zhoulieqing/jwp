package com.tbynet.jwp.service;

import java.util.List;

import com.tbynet.jwp.model.Options;

public interface OptionService {

	public boolean save(Object...options);
	
	public boolean update(Object...options);
	
	public Options getOption(String name);
	
	public List<Options> getOptions(String...names);

	public String get_option(String optionName);
	
}
