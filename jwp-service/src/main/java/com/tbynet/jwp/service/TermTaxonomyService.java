package com.tbynet.jwp.service;

import com.tbynet.jwp.model.TermTaxonomy;

public interface TermTaxonomyService {
	
	public TermTaxonomy getTermTaxonomy(Object termTaxonomyId);

	public TermTaxonomy getTermTaxonomy(Object termId, String taxonomy);
	
}
