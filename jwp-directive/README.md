### 包含模板文件的标签（Include tags）
- get_header
- get_sidebar
- get_search_form
- comments_template
- get_footer

### 博客信息标签（Blog info tags）
- bloginfo
- bloginfo_rss
- get_bloginfo
- get_bloginfo_rss

### 列表及下拉列表的标签（Lists & Dropdown tags）
- wp_list_authors
- wp_list_categories
- wp_list_pages
- wp_list_bookmarks
- wp_list_comments
- wp_get_archives
- wp_page_menu
- wp_dropdown_pages
- wp_dropdown_categories
- wp_dropdown_users

### 登录/登出标签（Login/Logout tags）
- is_user_logged_in
- wp_login_url
- wp_logout_url
- wp_lostpassword_url
- wp_registration_url
- wp_loginout
- wp_register
- wp_signon

### 文章信息标签（Post tags）
- the_ID
- the_title
- the_title_rss
- the_title_attribute
- single_post_title
- the_content
- the_content_rss
- the_excerpt
- the_excerpt_rss
- wp_link_pages
- posts_nav_link
- next_post_link
- next_posts_link
- previous_post_link
- previous_posts_link
- next_image_link
- previous_image_link
- sticky_class
- the_category
- the_category_rss
- the_tags
- the_meta

### 评论标签（Comment tags）
- wp_list_comments
- comments_number
- comments_link
- comments_rss_link
- comments_popup_script
- comments_popup_link
- comment_ID
- comment_id_fields
- comment_author
- comment_author_link
- comment_author_email
- comment_author_email_link
- comment_author_url
- comment_author_url_link
- comment_author_IP
- comment_type
- comment_text
- comment_excerpt
- comment_date
- comment_time
- comment_form_title
- comment_author_rss
- comment_text_rss
- comment_link_rss
- permalink_comments_rss
- comment_reply_link
- cancel_comment_reply_link
- previous_comments_link
- next_comments_link
- paginate_comments_links

### 时间及日期标签（Date and Time tags）
- the_time
- the_date
- the_date_xml
- the_modified_time
- the_modified_date
- the_modified_author
- single_month_title
- get_the_time
- get_day_link
- get_month_link
- get_year_link
- get_calendar

### 分类标签（Category tags）
- is_category
- the_category
- the_category_rss
- single_cat_title
- category_description
- wp_dropdown_categories
- wp_list_categories
- get_category_parents
- get_the_category
- get_category_link
- in_category

### 作者信息标签（Author tags）
- the_author
- the_author_link
- the_author_posts
- the_author_posts_link
- the_author_meta
- wp_list_authors
- wp_dropdown_users

### 文章 Tag 标签（Tag tags）
- is_tag
- the_tags
- tag_description
- single_tag_title
- wp_tag_cloud
- wp_generate_tag_cloud
- get_the_tags
- get_the_tag_list
- get_tag_link

### 编辑链接标签（Edit Link tags）
- edit_post_link
- edit_comment_link
- edit_tag_link
- edit_bookmark_link

### 固定链接标签（Permalink tags）
- permalink_anchor
- get_permalink
- the_permalink
- permalink_single_rss

### 链接管理标签（Links Manager tags）
- wp_list_bookmarks
- get_bookmarks
- get_bookmark
- get_bookmark_field

### 引用标签（Trackback tags）
- trackback_url
- trackback_rdf

### 一般标签（General tags）
- wp_title
- get_posts
- query_posts
- the_search_query

参考[模板标签](https://codex.wordpress.org/zh-cn:模板标签)