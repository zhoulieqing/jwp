package com.tbynet.jwp.portal;

import com.jfinal.core.Action;
import com.jfinal.core.JFinal;
import com.jfinal.handler.Handler;

public class PortalHandler extends Handler {

    @Override
    public void handle(String target, javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response, boolean[] isHandled) {
        String[] urlPara = {null};
        Action action = JFinal.me().getAction(target, urlPara);
        if(null == action) {

        }
        next.handle(target, request, response, isHandled);
    }
}
