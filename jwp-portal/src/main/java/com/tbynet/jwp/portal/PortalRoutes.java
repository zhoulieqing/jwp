package com.tbynet.jwp.portal;

import com.jfinal.config.Routes;
import com.tbynet.jwp.portal.index.IndexPortalController;

public class PortalRoutes extends Routes {

	@Override
	public void config() {
		addInterceptor(new PortalInterceptor());
		setBaseViewPath("/portal");
		
		add("/", IndexPortalController.class);
	}

}
