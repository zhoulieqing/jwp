package com.tbynet.jwp.portal.index;

import com.tbynet.jwp.portal.PortalController;

public class IndexPortalController extends PortalController {

	public void index() {
		
		render("index.html");
	}
	
	public void category() {
		
		render("category.html");
	}
	
	public void post() {
		
		render("post.html");
	}
	
}
