# Jwp（正在进行时）

#### 介绍

![logo](https://gitee.com/uploads/images/2019/0407/162153_17992f83_1176.png "logo.png")

Jwp是基于JFinal开发的WordPress（Java）系统，数据库脚本直接用的是WordPress（PHP） 4.9版系统的脚本，数据存储方式也是严格按照WorderPress（PHP）4.9版系统的方式来实现的，这样也就意味着之前用WordPress（PHP）4.9版系统的小伙伴们将来也可以试用本系统（计划正在进行时）。
本系统是在数据模型不变的情况下，尽量还原WordPress（PHP） 4.9版系统的功能。

#### 访问示例

- 前台: http://jwp.tbynet.com
- 后台: http://jwp.tbynet.com/admin 用户名：Adam, 密码：tbynet

#### 软件架构
JFinal 4.9.16 + MySQL 5.7.16 + Layui 2.6.8

- jwp                       --父级模块
- jwp-framework             --框架模块
- jwp-plugin                --插件模块
- jwp-model                 --模型模块
- jwp-service               --服务接口模块
- jwp-service-provider      --服务提供模块
- jwp-directive             --指令模块
- jwp-admin                 --后台控制模块
- jwp-portal                --前台控制模块
- jwp-web                   --WEB模块

#### 安装教程

1. IntelliJ IDEA 2021.2.3 (Community Edition)
2. JDK 1.8
3. 数据库脚本在根目录 wordpress.sql，可以直接导入脚本
4. 找到/jwp/jwp-web/src/main/resources/config.dev.properties，更改数据库用户名和密码
5. 如果有需要更改系统访问端口，请找到/jwp/jwp-web/src/main/resources/undertow.txt，更改undertow.port参数
6. 运行/调试时，更改IDEA的运行/调试配置的工作目录(W)为%MODULE_WORKING_DIR%

#### 使用说明

1. cd wordpress
2. mvn clean package
3. 找到jwp-web/src/main/java/com/tbynet/jwp/web/Application.java，执行 Java Application Run 命令
4. 访问 http://localhost (前台） http://localhost/admin (后台) 用户名：Adam, 密码：tbynet

#### 项目进度

- 2019-04-02: 完成分类目录和标签添加、编辑功能
- 2019-03-29: 完成设置功能
- 2019-03-28: 完成添加用户，编辑用户功能
- 2019-03-26: 后台页面制作，完成用户登录功能，更新个人资料功能，搜索用户功能
- 2019-03-21: 搭建项目框架

#### 项目计划

- 接口定义
- 后台页面制作，基于layui仿造WordPress（PHP）的后台页面
- 用户登录功能
- 个人资料更新功能
- 实现用户模块
- 实现设置模块
- 实现文章模块
- 实现媒体模块
- 实现页面模块
- 实现评论模块
- 实现菜单功能
- 实现指令模块（模板标签功能）
- 实现主题功能
- 实现导入WordPress（PHP）模板功能

#### 参与贡献

欢迎大家一起参与这个项目的开发实践中，QQ：348672425 

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request

#### 参考资料

- JFinal: [JFinal文档](http://www.jfinal.com/doc)
- Layui: [Layui文档](http://www.layui.com/doc)
- WordPress: [理解和利用 WordPress 中的数据](https://www.wpdaxue.com/understanding-and-working-with-data-in-wordpress.html)

#### 系统截图(是不是似从相识的感觉，哈哈)

![输入图片说明](https://gitee.com/uploads/images/2019/0407/170022_b75855c9_1176.png "屏幕快照 2019-04-07 16.56.11.png")

![输入图片说明](https://gitee.com/uploads/images/2019/0407/170042_ad373f8e_1176.png "屏幕快照 2019-04-07 16.56.50.png")

![输入图片说明](https://gitee.com/uploads/images/2019/0407/170058_f217b915_1176.png "屏幕快照 2019-04-07 16.57.09.png")

![输入图片说明](https://gitee.com/uploads/images/2019/0407/170110_0cbfc9fe_1176.png "屏幕快照 2019-04-07 16.57.35.png")

![输入图片说明](https://gitee.com/uploads/images/2019/0407/170130_b57b7f53_1176.png "屏幕快照 2019-04-07 16.57.56.png")

![输入图片说明](https://gitee.com/uploads/images/2019/0407/170149_efc92f9f_1176.png "屏幕快照 2019-04-07 16.58.10.png")

![输入图片说明](https://gitee.com/uploads/images/2019/0407/170209_79e6abe6_1176.png "屏幕快照 2019-04-07 16.58.29.png")

![输入图片说明](https://gitee.com/uploads/images/2019/0407/170229_42c855c1_1176.png "屏幕快照 2019-04-07 16.59.43.png")

![输入图片说明](https://gitee.com/uploads/images/2019/0407/170241_97c285eb_1176.png "屏幕快照 2019-04-07 16.59.21.png")
