package com.tbynet.jwp.admin.option;

import java.util.ArrayList;
import java.util.List;

import com.jfinal.aop.Inject;
import com.jfinal.kit.Ret;
import com.jfinal.kit.StrKit;
import com.tbynet.jwp.admin.AdminController;
import com.tbynet.jwp.model.Options;
import com.tbynet.jwp.service.OptionService;

public class OptionAdminController extends AdminController {
	
	@Inject
	OptionService optionService;

	public void index() {
		render("index.html");
	}
	
	public void general() {
		if(isGet()) {
			String[] names = {Options.blogname, Options.blogdescription, Options.siteurl, 
					Options.home, Options.new_admin_email, Options.users_can_register,
					Options.default_role, Options.WPLANG, Options.timezone_string, 
					Options.date_format, Options.time_format, Options.start_of_week, Options.zh_cn_l10n_icp_num};
			
			List<Options> options = optionService.getOptions(names);
			for(Options option : options) {
				if(null != option.getOptionValue()) {
					set(option.getOptionName(), option.getOptionValue());
				}
			}
			
			render("general.html");
			return;
		}
		
		List<Options> options = new ArrayList<Options>();
		
		String blogname = getPara("blogname", "");
		if(null != blogname) {
			Options option = optionService.getOption(Options.blogname);
			option.setBlogName(blogname);
			options.add(option);
		}
		String blogdescription = getPara("blogdescription", "");
		if(null != blogdescription) {
			Options option = optionService.getOption(Options.blogdescription);
			option.setBlogDescription(blogdescription);
			options.add(option);
		}
		String siteurl = getPara("siteurl", "");
		if(null != siteurl) {
			Options option = optionService.getOption(Options.siteurl);
			option.setSiteUrl(siteurl);
			options.add(option);
		}
		String home = getPara("home", "");
		if(null != home) {
			Options option = optionService.getOption(Options.home);
			option.setHome(home);
			options.add(option);
		}
		String new_admin_email = getPara("new_admin_email", "");
		if(null != new_admin_email) {
			Options option = optionService.getOption(Options.new_admin_email);
			option.setNewAdminEmail(new_admin_email);
			options.add(option);
		}
		String users_can_register = getPara("users_can_register", "0");
		if(StrKit.notBlank(users_can_register)) {
			Options option = optionService.getOption(Options.users_can_register);
			option.setUsersCanRegister(users_can_register);
			options.add(option);
		}
		String default_role = getPara("default_role");
		if(StrKit.notBlank(default_role)) {
			Options option = optionService.getOption(Options.default_role);
			option.setDefaultRole(default_role);
			options.add(option);
		}
		String WPLANG = getPara("WPLANG");
		if(StrKit.notBlank(WPLANG)) {
			Options option = optionService.getOption(Options.WPLANG);
			option.setWpLang(WPLANG);
			options.add(option);
		}
		String timezone_string = getPara("timezone_string");
		if(StrKit.notBlank(timezone_string)) {
			Options option = optionService.getOption(Options.timezone_string);
			option.setTimezoneString(timezone_string);
			options.add(option);
		}
		String date_format = getPara("date_format_custom", "");
		if(null != date_format) {
			Options option = optionService.getOption(Options.date_format);
			option.setDateFormat(date_format);
			options.add(option);
		}
		String time_format = getPara("time_format_custom", "");
		if(null != time_format) {
			Options option = optionService.getOption(Options.time_format);
			option.setTimeFormat(time_format);
			options.add(option);
		}
		String start_of_week = getPara("start_of_week");
		if(StrKit.notBlank(start_of_week)) {
			Options option = optionService.getOption(Options.start_of_week);
			option.setStartOfWeek(start_of_week);
			options.add(option);
		}
		String zh_cn_l10n_icp_num = getPara("zh_cn_l10n_icp_num", "");
		if(null != zh_cn_l10n_icp_num) {
			Options option = optionService.getOption(Options.zh_cn_l10n_icp_num);
			option.setZhCnL10nIcpNum(zh_cn_l10n_icp_num);
			options.add(option);
		}
		
		if(optionService.update(options.toArray())) {
			ok(Ret.by("msg", "设置已保存"));
		} else {
			fail("设置失败，请联系管理员");
		}
	}
	
	public void writing() {
		if(isGet()) {
			String[] names = {Options.default_category, Options.default_post_format, Options.mailserver_url,
					Options.mailserver_port, Options.mailserver_login, Options.mailserver_pass,
					Options.default_email_category, Options.ping_sites};
			
			List<Options> options = optionService.getOptions(names);
			for(Options option : options) {
				if(null != option.getOptionValue()) {
					set(option.getOptionName(), option.getOptionValue());
				}
			}
			render("writing.html");
			return;
		}
		
		List<Options> options = new ArrayList<Options>();
		
		String default_category = getPara("default_category");
		if(StrKit.notBlank(default_category)) {
			Options option = optionService.getOption(Options.default_category);
			option.setDefaultCategory(default_category);
			options.add(option);
		}
		String default_post_format = getPara("default_post_format");
		if(StrKit.notBlank(default_post_format)) {
			Options option = optionService.getOption(Options.default_post_format);
			option.setDefaultPostFormat(default_post_format);
			options.add(option);
		}
		String mailserver_url = getPara("mailserver_url", "");
		if(null != mailserver_url) {
			Options option = optionService.getOption(Options.mailserver_url);
			option.setMailServerUrl(mailserver_url);
			options.add(option);
		}
		String mailserver_port = getPara("mailserver_port", "");
		if(null != mailserver_port) {
			Options option = optionService.getOption(Options.mailserver_port);
			option.setMailServerPort(mailserver_port);
			options.add(option);
		}
		String mailserver_login = getPara("mailserver_login", "");
		if(null != mailserver_login) {
			Options option = optionService.getOption(Options.mailserver_login);
			option.setMailServerLogin(mailserver_login);
			options.add(option);
		}
		String mailserver_pass = getPara("mailserver_pass", "");
		if(null != mailserver_pass) {
			Options option = optionService.getOption(Options.mailserver_pass);
			option.setMailServerPass(mailserver_pass);
			options.add(option);
		}
		String default_email_category = getPara("default_email_category");
		if(StrKit.notBlank(default_email_category)) {
			Options option = optionService.getOption(Options.default_email_category);
			option.setDefaultEmailCategory(default_email_category);
			options.add(option);
		}
		String ping_sites = getPara("ping_sites", "");
		if(null != ping_sites) {
			Options option = optionService.getOption(Options.ping_sites);
			option.setPingSites(ping_sites);
			options.add(option);
		}
		
		if(optionService.update(options.toArray())) {
			ok(Ret.by("msg", "设置已保存"));
		} else {
			fail("设置失败，请联系管理员");
		}
	}
	
	public void reading() {
		if(isGet()) {
			String[] names = {Options.show_on_front, Options.page_on_front, Options.page_for_posts,
					Options.posts_per_page, Options.posts_per_rss, Options.rss_use_excerpt, Options.blog_public};
			
			List<Options> options = optionService.getOptions(names);
			for(Options option : options) {
				if(null != option.getOptionValue()) {
					set(option.getOptionName(), option.getOptionValue());
				}
			}
			render("reading.html");
			return;
		}
		
		List<Options> options = new ArrayList<Options>();
		
		String show_on_front = getPara("show_on_front");
		if(StrKit.notBlank(show_on_front)) {
			Options option = optionService.getOption(Options.show_on_front);
			option.setShowOnFront(show_on_front);
			options.add(option);
		}
		String page_on_front = getPara("page_on_front");
		if(StrKit.notBlank(page_on_front)) {
			Options option = optionService.getOption(Options.page_on_front);
			option.setPageOnFront(page_on_front);
			options.add(option);
		}
		String page_for_posts = getPara("page_for_posts");
		if(StrKit.notBlank(page_for_posts)) {
			Options option = optionService.getOption(Options.page_for_posts);
			option.setPageForPosts(page_for_posts);
			options.add(option);
		}
		String posts_per_page = getPara("posts_per_page", "1");
		if(StrKit.notBlank(posts_per_page)) {
			Options option = optionService.getOption(Options.posts_per_page);
			option.setPostsPerPage(posts_per_page);
			options.add(option);
		}
		String posts_per_rss = getPara("posts_per_rss", "1");
		if(StrKit.notBlank(posts_per_rss)) {
			Options option = optionService.getOption(Options.posts_per_rss);
			option.setPostsPerRSS(posts_per_rss);
			options.add(option);
		}
		String rss_use_excerpt = getPara("rss_use_excerpt");
		if(StrKit.notBlank(rss_use_excerpt)) {
			Options option = optionService.getOption(Options.rss_use_excerpt);
			option.setRSSUseExcerpt(rss_use_excerpt);
			options.add(option);
		}
		String blog_public = getPara("blog_public");
		if(StrKit.notBlank(blog_public)) {
			Options option = optionService.getOption(Options.blog_public);
			option.setBlogPublic(blog_public);
			options.add(option);
		}
		
		if(optionService.update(options.toArray())) {
			ok(Ret.by("msg", "设置已保存"));
		} else {
			fail("设置失败，请联系管理员");
		}
	}
	
	public void discussion() {
		if(isGet()) {
			String[] names = {Options.default_pingback_flag, Options.default_ping_status, Options.default_comment_status,
					Options.require_name_email, Options.comment_registration, Options.close_comments_for_old_posts,
					Options.close_comments_days_old, Options.show_comments_cookies_opt_in, Options.thread_comments,
					Options.thread_comments_depth, Options.page_comments, Options.comments_per_page, Options.default_comments_page,
					Options.comment_order, Options.comments_notify, Options.moderation_notify, Options.comment_moderation,
					Options.comment_whitelist, Options.comment_max_links, Options.moderation_keys, Options.blacklist_keys,
					Options.show_avatars, Options.avatar_rating, Options.avatar_default};
			
			List<Options> options = optionService.getOptions(names);
			for(Options option : options) {
				if(null != option.getOptionValue()) {
					set(option.getOptionName(), option.getOptionValue());
				}
			}
			render("discussion.html");
			return;
		}
		
		List<Options> options = new ArrayList<Options>();
		
		String default_pingback_flag = getPara("default_pingback_flag", "0");
		if(StrKit.notBlank(default_pingback_flag)) {
			Options option = optionService.getOption(Options.default_pingback_flag);
			option.setDefaultPingbackFlag(default_pingback_flag);
			options.add(option);
		}
		String default_ping_status = getPara("default_ping_status", "closed");
		if(StrKit.notBlank(default_ping_status)) {
			Options option = optionService.getOption(Options.default_ping_status);
			option.setDefaultPingStatus(default_ping_status);
			options.add(option);
		}
		String default_comment_status = getPara("default_comment_status", "closed");
		if(StrKit.notBlank(default_comment_status)) {
			Options option = optionService.getOption(Options.default_comment_status);
			option.setDefaultCommentStatus(default_comment_status);
			options.add(option);
		}
		String require_name_email = getPara("require_name_email", "0");
		if(StrKit.notBlank(require_name_email)) {
			Options option = optionService.getOption(Options.require_name_email);
			option.setRequireNameEmail(require_name_email);
			options.add(option);
		}
		String comment_registration = getPara("comment_registration", "0");
		if(StrKit.notBlank(comment_registration)) {
			Options option = optionService.getOption(Options.comment_registration);
			option.setCommentRegistration(comment_registration);
			options.add(option);
		}
		String close_comments_for_old_posts = getPara("close_comments_for_old_posts", "0");
		if(StrKit.notBlank(close_comments_for_old_posts)) {
			Options option = optionService.getOption(Options.close_comments_for_old_posts);
			option.setCloseCommentsForOldPosts(close_comments_for_old_posts);
			options.add(option);
		}
		String close_comments_days_old = getPara("close_comments_days_old", "0");
		if(StrKit.notBlank(close_comments_days_old)) {
			Options option = optionService.getOption(Options.close_comments_days_old);
			option.setCloseCommentsDaysOld(close_comments_days_old);
			options.add(option);
		}
		String show_comments_cookies_opt_in = getPara("show_comments_cookies_opt_in", "0");
		if(StrKit.notBlank(show_comments_cookies_opt_in)) {
			Options option = optionService.getOption(Options.show_comments_cookies_opt_in);
			option.setShowCommentsCookiesOptIn(show_comments_cookies_opt_in);
			options.add(option);
		}
		String thread_comments = getPara("thread_comments", "0");
		if(StrKit.notBlank(thread_comments)) {
			Options option = optionService.getOption(Options.thread_comments);
			option.setThreadComments(thread_comments);
			options.add(option);
		}
		String thread_comments_depth = getPara("thread_comments_depth");
		if(StrKit.notBlank(thread_comments_depth)) {
			Options option = optionService.getOption(Options.thread_comments_depth);
			option.setThreadCommentsDepth(thread_comments_depth);
			options.add(option);
		}
		String page_comments = getPara("page_comments", "0");
		if(StrKit.notBlank(page_comments)) {
			Options option = optionService.getOption(Options.page_comments);
			option.setPageComments(page_comments);
			options.add(option);
		}
		String comments_per_page = getPara("comments_per_page", "0");
		if(StrKit.notBlank(comments_per_page)) {
			Options option = optionService.getOption(Options.comments_per_page);
			option.setCommentsPerPage(comments_per_page);
			options.add(option);
		}
		String default_comments_page = getPara("default_comments_page");
		if(StrKit.notBlank(default_comments_page)) {
			Options option = optionService.getOption(Options.default_comments_page);
			option.setDefaultCommentspage(default_comments_page);
			options.add(option);
		}
		String comment_order = getPara("comment_order");
		if(StrKit.notBlank(comment_order)) {
			Options option = optionService.getOption(Options.comment_order);
			option.setCommentOrder(comment_order);
			options.add(option);
		}
		String comments_notify = getPara("comments_notify", "0");
		if(StrKit.notBlank(comments_notify)) {
			Options option = optionService.getOption(Options.comments_notify);
			option.setCommentsNotify(comments_notify);
			options.add(option);
		}
		String moderation_notify = getPara("moderation_notify", "0");
		if(StrKit.notBlank(moderation_notify)) {
			Options option = optionService.getOption(Options.moderation_notify);
			option.setModerationNotify(moderation_notify);
			options.add(option);
		}
		String comment_moderation = getPara("comment_moderation", "0");
		if(StrKit.notBlank(comment_moderation)) {
			Options option = optionService.getOption(Options.comment_moderation);
			option.setCommentModeration(comment_moderation);
			options.add(option);
		}
		String comment_whitelist = getPara("comment_whitelist", "0");
		if(StrKit.notBlank(comment_whitelist)) {
			Options option = optionService.getOption(Options.comment_whitelist);
			option.setCommentWhitelist(comment_whitelist);
			options.add(option);
		}
		String comment_max_links = getPara("comment_max_links", "0");
		if(StrKit.notBlank(comment_max_links)) {
			Options option = optionService.getOption(Options.comment_max_links);
			option.setCommentMaxLinks(comment_max_links);
			options.add(option);
		}
		String moderation_keys = getPara("moderation_keys", "");
		if(null != moderation_keys) {
			Options option = optionService.getOption(Options.moderation_keys);
			option.setModerationKeys(moderation_keys);
			options.add(option);
		}
		String blacklist_keys = getPara("blacklist_keys", "");
		if(null != blacklist_keys) {
			Options option = optionService.getOption(Options.blacklist_keys);
			option.setBlacklistKeys(blacklist_keys);
			options.add(option);
		}
		String show_avatars = getPara("show_avatars", "0");
		if(StrKit.notBlank(show_avatars)) {
			Options option = optionService.getOption(Options.show_avatars);
			option.setShowAvatars(show_avatars);
			options.add(option);
		}
		String avatar_rating = getPara("avatar_rating");
		if(StrKit.notBlank(avatar_rating)) {
			Options option = optionService.getOption(Options.avatar_rating);
			option.setAvatarRating(avatar_rating);
			options.add(option);
		}
		String avatar_default = getPara("avatar_default");
		if(StrKit.notBlank(avatar_default)) {
			Options option = optionService.getOption(Options.avatar_default);
			option.setAvatarDefault(avatar_default);
			options.add(option);
		}
		
		if(optionService.update(options.toArray())) {
			ok(Ret.by("msg", "设置已保存"));
		} else {
			fail("设置失败，请联系管理员");
		}
	}
	
	public void media() {
		if(isGet()) {
			String[] names = {Options.thumbnail_size_w, Options.thumbnail_size_h, Options.thumbnail_crop,
					Options.medium_size_w, Options.medium_size_h, Options.large_size_w, Options.large_size_h, 
					Options.uploads_use_yearmonth_folders};
			
			List<Options> options = optionService.getOptions(names);
			for(Options option : options) {
				if(null != option.getOptionValue()) {
					set(option.getOptionName(), option.getOptionValue());
				}
			}
			render("media.html");
			return;
		}
		
		List<Options> options = new ArrayList<Options>();
		
		String thumbnail_size_w = getPara("thumbnail_size_w", "0");
		if(null != thumbnail_size_w) {
			Options option = optionService.getOption(Options.thumbnail_size_w);
			option.setThumbnailSizeW(thumbnail_size_w);
			options.add(option);
		}
		String thumbnail_size_h = getPara("thumbnail_size_h", "0");
		if(null != thumbnail_size_h) {
			Options option = optionService.getOption(Options.thumbnail_size_h);
			option.setThumbnailSizeH(thumbnail_size_h);
			options.add(option);
		}
		String thumbnail_crop = getPara("thumbnail_crop", "0");
		if(StrKit.notBlank(thumbnail_crop)) {
			Options option = optionService.getOption(Options.thumbnail_crop);
			option.setThumbnailCrop(thumbnail_crop);
			options.add(option);
		}
		String medium_size_w = getPara("medium_size_w", "0");
		if(null != medium_size_w) {
			Options option = optionService.getOption(Options.medium_size_w);
			option.setMediumSizeW(medium_size_w);
			options.add(option);
		}
		String medium_size_h = getPara("medium_size_h", "0");
		if(null != medium_size_h) {
			Options option = optionService.getOption(Options.medium_size_h);
			option.setMediumSizeH(medium_size_h);
			options.add(option);
		}
		String large_size_w = getPara("large_size_w", "0");
		if(null != large_size_w) {
			Options option = optionService.getOption(Options.large_size_w);
			option.setLargeSizeW(large_size_w);
			options.add(option);
		}
		String large_size_h = getPara("large_size_h", "0");
		if(null != large_size_h) {
			Options option = optionService.getOption(Options.large_size_h);
			option.setLargeSizeH(large_size_h);
			options.add(option);
		}
		String uploads_use_yearmonth_folders = getPara("uploads_use_yearmonth_folders", "0");
		if(StrKit.notBlank(uploads_use_yearmonth_folders)) {
			Options option = optionService.getOption(Options.uploads_use_yearmonth_folders);
			option.setUploadsUseYearmonthFolder(uploads_use_yearmonth_folders);
			options.add(option);
		}
		
		if(optionService.update(options.toArray())) {
			ok(Ret.by("msg", "设置已保存"));
		} else {
			fail("设置失败，请联系管理员");
		}
	}
	
	public void permalink() {
		if(isGet()) {
			String[] names = {Options.permalink_structure, Options.category_base, Options.tag_base};
			
			List<Options> options = optionService.getOptions(names);
			for(Options option : options) {
				if(null != option.getOptionValue()) {
					set(option.getOptionName(), option.getOptionValue());
				}
			}
			render("permalink.html");
			return;
		}
		
		List<Options> options = new ArrayList<Options>();
		
		String permalink_structure = getPara("permalink_structure", "");
		if(null != permalink_structure) {
			Options option = optionService.getOption(Options.permalink_structure);
			option.setPermalinkStructure(permalink_structure);
			options.add(option);
		}
		String category_base = getPara("category_base", "");
		if(null != category_base) {
			Options option = optionService.getOption(Options.category_base);
			option.setCategoryBase(category_base);
			options.add(option);
		}
		String tag_base = getPara("tag_base", "");
		if(null != tag_base) {
			Options option = optionService.getOption(Options.tag_base);
			option.setTagBase(tag_base);
			options.add(option);
		}
		
		if(optionService.update(options.toArray())) {
			ok(Ret.by("msg", "设置已保存"));
		} else {
			fail("设置失败，请联系管理员");
		}
	}
	
	public void privacy() {
		if(isGet()) {
			String[] names = {Options.wp_page_for_privacy_policy};
			
			List<Options> options = optionService.getOptions(names);
			for(Options option : options) {
				if(null != option.getOptionValue()) {
					set(option.getOptionName(), option.getOptionValue());
				}
			}
			render("privacy.html");
			return;
		}
		
		List<Options> options = new ArrayList<Options>();
		
		String wp_page_for_privacy_policy = getPara("page_for_privacy_policy", "");
		if(null != wp_page_for_privacy_policy) {
			Options option = optionService.getOption(Options.wp_page_for_privacy_policy);
			option.setWpPageForPrivacyPolicy(wp_page_for_privacy_policy);
			options.add(option);
		}
		
		if(optionService.update(options.toArray())) {
			ok(Ret.by("msg", "设置已保存"));
		} else {
			fail("设置失败，请联系管理员");
		}
	}
}
