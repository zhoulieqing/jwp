package com.tbynet.jwp.admin;

import com.tbynet.jwp.framework.core.JwpController;

public class AdminController extends JwpController {

	public int getPage() {
		return getInt("page", 1);
	}
	
	public int getSize() {
		return getInt("size", 20);
	}
}
