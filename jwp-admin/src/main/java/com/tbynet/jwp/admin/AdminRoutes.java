package com.tbynet.jwp.admin;

import com.jfinal.config.Routes;
import com.tbynet.jwp.admin.attachment.AttachmentAdminController;
import com.tbynet.jwp.admin.category.CategoryAdminController;
import com.tbynet.jwp.admin.comment.CommentAdminController;
import com.tbynet.jwp.admin.index.IndexAdminController;
import com.tbynet.jwp.admin.menu.MenuAdminController;
import com.tbynet.jwp.admin.option.OptionAdminController;
import com.tbynet.jwp.admin.page.PageAdminController;
import com.tbynet.jwp.admin.plugin.PluginAdminController;
import com.tbynet.jwp.admin.post.PostAdminController;
import com.tbynet.jwp.admin.profile.ProfileAdminController;
import com.tbynet.jwp.admin.tag.TagAdminController;
import com.tbynet.jwp.admin.theme.ThemeAdminController;
import com.tbynet.jwp.admin.user.UserAdminController;

public class AdminRoutes extends Routes {

	@Override
	public void config() {
		addInterceptor(new AdminInterceptor());
		setBaseViewPath("/WEB-INF/admin");
		
		add("/admin", IndexAdminController.class, "/index");
		add("/admin/post", PostAdminController.class, "/post");
		add("/admin/page", PageAdminController.class, "/page");
		add("/admin/category", CategoryAdminController.class, "/category");
		add("/admin/tag", TagAdminController.class, "/tag");
		add("/admin/attachment", AttachmentAdminController.class, "/attachment");
		add("/admin/comment", CommentAdminController.class, "/comment");
		add("/admin/user", UserAdminController.class, "/user");
		add("/admin/profile", ProfileAdminController.class, "/profile");
		add("/admin/option", OptionAdminController.class, "/option");
		add("/admin/plugin", PluginAdminController.class, "/plugin");
		add("/admin/theme", ThemeAdminController.class, "/theme");
		add("/admin/menu", MenuAdminController.class, "/menu");
	}

}
