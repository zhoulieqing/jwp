package com.tbynet.jwp.admin.page;

import com.tbynet.jwp.admin.AdminController;

public class PageAdminController extends AdminController {

	public void index() {
		render("index.html");
	}
	
	public void create() {
		render("create.html");
	}
	
	public void edit() {
		render("edit.html");
	}
	
	public void save() {
		
	}
	
	public void update() {
		
	}
}
