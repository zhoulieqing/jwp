package com.tbynet.jwp.admin.index;

import com.jfinal.aop.Clear;
import com.jfinal.aop.Inject;
import com.jfinal.kit.Ret;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.ehcache.CacheKit;
import com.tbynet.jwp.admin.AdminController;
import com.tbynet.jwp.admin.AdminInterceptor;
import com.tbynet.jwp.framework.kit.IpKit;
import com.tbynet.jwp.model.Users;
import com.tbynet.jwp.service.UserService;

public class IndexAdminController extends AdminController {

	@Inject
	UserService userService;
	
	public void index() {
		render("index.html");
	}
	
	@Clear(AdminInterceptor.class)
	public void captcha() {
		renderCaptcha();
	}
	
	@Clear(AdminInterceptor.class)
	public void login() {
		String redirectUrl = getPara("redirect_url", "/admin");
		if(isGet()) {
			String sessionId = getCookie(Users.sessionIdName);
			removeCookie(Users.sessionIdName);
			userService.logout(sessionId);
			set("redirectUrl", redirectUrl);
			render("login.html");
			return;
		}
		
		String username = getPara("username");
		String password = getPara("password");
		String vcode = getPara("vcode");
		boolean keepLogin = getParaToBoolean("rememberme", false);
		
		if(StrKit.isBlank(username)) {
			fail("用户名或电子邮件地址不能为空");
			return;
		}
		if(StrKit.isBlank(password)) {
			fail("密码不能为空");
			return;
		}
		if(StrKit.isBlank(vcode)) {
			fail("验证码不能为空");
			return;
		}
		
		if(false == validateCaptcha("vcode")) {
			fail("验证码不正确");
			return;
		}
		
		String loginIp = IpKit.getRealIp(getRequest());
		Ret ret = userService.login(username, password, keepLogin, loginIp, getUserAgent());
		if(ret.isOk()) {
			String sessionId = ret.getStr(Users.sessionIdName);
			int maxAgeInSeconds = ret.getAs("maxAgeInSeconds");
			setCookie(Users.sessionIdName, sessionId, maxAgeInSeconds, true);
			setAttr(Users.loginUserCacheName, ret.get(Users.loginUserCacheName));

			ret.set("redirectUrl", redirectUrl);    // 如果 redirectUrl 存在则跳过去，否则跳去首页
		}
		
		json(ret);
	}
	
	public void logout() {
		String sessionId = getCookie(Users.sessionIdName);
		removeCookie(Users.sessionIdName);
		userService.logout(sessionId);
		redirect("/admin/login");
	}
	
	@Clear(AdminInterceptor.class)
	public void register() {
		if(isGet()) {
			render("register.html");
			return;
		}
		
		String username = getPara("username");
		String vcode = getPara("vcode");
		
		if(StrKit.isBlank(username)) {
			fail("用户名或电子邮件地址不能为空");
			return;
		}
		if(StrKit.isBlank(vcode)) {
			fail("验证码不能为空");
			return;
		}
		
		if(false == validateCaptcha("vcode")) {
			fail("验证码不正确");
			return;
		}
		
		ok(Ret.by("msg", "发送成功"));
	}
	
	@Clear(AdminInterceptor.class)
	public void forget() {
		if(isGet()) {
			render("forget.html");
			return;
		}
		
		String username = getPara("username");
		String vcode = getPara("vcode");
		
		if(StrKit.isBlank(username)) {
			fail("用户名或电子邮件地址不能为空");
			return;
		}
		if(StrKit.isBlank(vcode)) {
			fail("验证码不能为空");
			return;
		}
		
		if(false == validateCaptcha("vcode")) {
			fail("验证码不正确");
			return;
		}
		
		ok(Ret.by("msg", "发送成功"));
	}
	
	/*
	 * 登出其他设备
	 */
	public void destroy() {
		String sessionId = getCookie(Users.sessionIdName);
		if(userService.destroy(sessionId)) {
			ok(Ret.by("msg", "登出其他设备成功"));
		} else {
			fail("登出其他设备失败，请联系管理员");
		}
	}
	
	@Clear(AdminInterceptor.class)
	public void clear() {
		CacheKit.removeAll(Users.loginUserCacheName);
		renderNull();
	}
}
