package com.tbynet.jwp.admin.plugin;

import com.tbynet.jwp.admin.AdminController;

public class PluginAdminController extends AdminController {

	public void index() {
		render("index.html");
	}
	
	public void create() {
		render("create.html");
	}
	
	public void edit() {
		render("edit.html");
	}
	
	public void save() {
		
	}
	
	public void update() {
		
	}
}
