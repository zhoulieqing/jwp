package com.tbynet.jwp.admin;

import com.jfinal.core.Action;
import com.jfinal.core.JFinal;
import com.jfinal.handler.Handler;

public class AdminHandler extends Handler {

    @Override
    public void handle(String target, javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response, boolean[] isHandled) {
        String[] urlPara = {null};
        Action action = JFinal.me().getAction(target, urlPara);
        if(null == action && target.startsWith("/admin")) {
            target = "/admin";
        }
        next.handle(target, request, response, isHandled);
    }
}
