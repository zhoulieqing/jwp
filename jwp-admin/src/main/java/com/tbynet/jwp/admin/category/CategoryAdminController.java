package com.tbynet.jwp.admin.category;

import java.math.BigInteger;

import com.jfinal.aop.Inject;
import com.jfinal.kit.Ret;
import com.jfinal.plugin.activerecord.Page;
import com.tbynet.jwp.admin.AdminController;
import com.tbynet.jwp.model.TermTaxonomy;
import com.tbynet.jwp.model.Terms;
import com.tbynet.jwp.service.TermService;
import com.tbynet.jwp.service.TermTaxonomyService;

public class CategoryAdminController extends AdminController {

	@Inject
	TermService termService;
	
	@Inject
	TermTaxonomyService taxonomyService;
	
	public void index() {
		set("parents", termService.getParents(TermTaxonomy.TAXONOMY_CATEGORY));
		
		render("index.html");
	}
	
	public void search() {
		String q = getPara("q");
		
		Page<Terms> page = termService.search(getPage(), getSize(), TermTaxonomy.TAXONOMY_CATEGORY, q);
		
		ok(Ret.by("data", page));
	}
	
	public void edit() {
		set("term", termService.getTerm(getPara("term_id"), TermTaxonomy.TAXONOMY_CATEGORY));
		set("parents", termService.getParents(TermTaxonomy.TAXONOMY_CATEGORY));
		
		render("edit.html");
	}
	
	public void save() {
		String name = getPara("name");
		String slug = getPara("slug");
		String description = getPara("description");
		String parent = getPara("parent", "0");
		
		Terms term = new Terms();
		term.setName(name);
		term.setSlug(slug);
		term.setTermGroup(0l);
		
		TermTaxonomy taxonomy = new TermTaxonomy();
		taxonomy.setTaxonomy(TermTaxonomy.TAXONOMY_CATEGORY);
		taxonomy.setDescription(description);
		taxonomy.setParent(new BigInteger(parent));
		taxonomy.setCount(0l);
		
		if(termService.save(term, taxonomy)) {
			ok(Ret.by("msg", "添加分类目录成功.").set("data", term.getTermId()));
		} else {
			fail("添加分类目录失败，请联系管理员.");
		}
	}
	
	public void update() {
		String termId = getPara("term_id");
		String name = getPara("name");
		String slug = getPara("slug");
		String description = getPara("description");
		String parent = getPara("parent", "0");
		
		Terms term = termService.getTerm(termId);
		term.setName(name);
		term.setSlug(slug);
		
		TermTaxonomy taxonomy = taxonomyService.getTermTaxonomy(term.getTermId(), TermTaxonomy.TAXONOMY_CATEGORY);
		taxonomy.setDescription(description);
		taxonomy.setParent(new BigInteger(parent));
		
		if(termService.update(term, taxonomy)) {
			ok(Ret.by("msg", "更新分类目录成功.").set("data", term.getTermId()));
		} else {
			fail("更新分类目录失败，请联系管理员.");
		}
	}
	
	public void delete() {
		
	}
}
