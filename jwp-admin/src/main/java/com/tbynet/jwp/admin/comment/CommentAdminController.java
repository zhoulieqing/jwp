package com.tbynet.jwp.admin.comment;

import com.tbynet.jwp.admin.AdminController;

public class CommentAdminController extends AdminController {

	public void index() {
		render("index.html");
	}
	
	public void create() {
		render("create.html");
	}
	
	public void edit() {
		render("edit.html");
	}
	
	public void save() {
		
	}
	
	public void update() {
		
	}
}
