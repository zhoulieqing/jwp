package com.tbynet.jwp.admin.profile;

import java.util.ArrayList;
import java.util.List;

import com.jfinal.aop.Inject;
import com.jfinal.kit.Ret;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.ehcache.CacheKit;
import com.tbynet.jwp.admin.AdminController;
import com.tbynet.jwp.framework.kit.PassKit;
import com.tbynet.jwp.model.Usermeta;
import com.tbynet.jwp.model.Users;
import com.tbynet.jwp.service.UserService;

public class ProfileAdminController extends AdminController {

	@Inject
	UserService userService;
	
	public void index() throws Exception {
		Users user = null;
		String sessionId = getCookie(Users.sessionIdName);
		if (null != sessionId) {
			user = userService.getLoginUserBySessionId(sessionId);
		}
		
		List<Usermeta> metas = userService.getUsermetas(user.getID());
		for(Usermeta meta : metas) {
			if(meta.getMetaKey().equals(Usermeta.session_tokens)) {
				user.put(meta.getMetaKey(), meta.getSessionTokens());
			} else {
				user.put(meta.getMetaKey(), meta.getMetaValue());
			}
		}
		
		set("user", user);
		
		render("index.html");
	}
	
	public void update() {
		Users user = null;
		String sessionId = getCookie(Users.sessionIdName);
		if (null != sessionId) {
			user = userService.getLoginUserBySessionId(sessionId);
		}
		
		String userId = get("user_id");
		String displayName = getPara("display_name");
		String email = getPara("email");
		String url = getPara("url", "");
		String userPass = getPara("user_pass");

		String firstName = getPara("first_name");
		String lastName = getPara("last_name");
		String nickname =getPara("nickname");
		String locale = getPara("locale");
		String description = getPara("description");
		String adminColor = getPara("admin_color");
		String richEditing = getPara("rich_editing");
		String syntaxHighlighting = getPara("syntax_highlighting");
		String commentShortcuts = getPara("comment_shortcuts");
		String adminBarFront = getPara("admin_bar_front");
		String simpleLocalAvatar = get("simple_local_avatar");
		
		//更新用户信息
		user.setDisplayName(displayName);
		user.setUserEmail(email);
		user.setUserUrl(url);
		if(StrKit.notBlank(userPass)) {
			userPass = PassKit.encrypt(userPass);
			user.setUserPass(userPass);
		}
		
		//更新用户元数据
		List<Usermeta> metas = new ArrayList<Usermeta>();
		
		if(StrKit.notBlank(firstName)) {
			Usermeta meta = userService.getUsermeta(user.getID(), Usermeta.first_name);
			if(null == meta) {
				meta = new Usermeta();
				meta.setUserId(user.getID());
			}
			meta.setFirstName(firstName);
			metas.add(meta);
		}
		if(StrKit.notBlank(lastName)) {
			Usermeta meta = userService.getUsermeta(user.getID(), Usermeta.last_name);
			if(null == meta) {
				meta = new Usermeta();
				meta.setUserId(user.getID());
			}
			meta.setLastName(lastName);
			metas.add(meta);
		}
		if(StrKit.notBlank(nickname)) {
			Usermeta meta = userService.getUsermeta(user.getID(), Usermeta.nickname);
			if(null == meta) {
				meta = new Usermeta();
				meta.setUserId(user.getID());
			}
			meta.setNickname(nickname);
			metas.add(meta);
		}
		if(StrKit.notBlank(locale)) {
			Usermeta meta = userService.getUsermeta(user.getID(), Usermeta.locale);
			if(null == meta) {
				meta = new Usermeta();
				meta.setUserId(user.getID());
			}
			meta.setLocale(locale);
			metas.add(meta);
		}
		if(StrKit.notBlank(description)) {
			Usermeta meta = userService.getUsermeta(user.getID(), Usermeta.description);
			if(null == meta) {
				meta = new Usermeta();
				meta.setUserId(user.getID());
			}
			meta.setDescription(description);
			metas.add(meta);
		}
		if(StrKit.notBlank(adminColor)) {
			Usermeta meta = userService.getUsermeta(user.getID(), Usermeta.admin_color);
			if(null == meta) {
				meta = new Usermeta();
				meta.setUserId(user.getID());
			}
			meta.setAdminColor(adminColor);
			metas.add(meta);
		}
		if(StrKit.notBlank(richEditing)) {
			Usermeta meta = userService.getUsermeta(user.getID(), Usermeta.rich_editing);
			if(null == meta) {
				meta = new Usermeta();
				meta.setUserId(user.getID());
			}
			meta.setRichEditing(richEditing);
			metas.add(meta);
		}
		if(StrKit.notBlank(syntaxHighlighting)) {
			Usermeta meta = userService.getUsermeta(user.getID(), Usermeta.syntax_highlighting);
			if(null == meta) {
				meta = new Usermeta();
				meta.setUserId(user.getID());
			}
			meta.setSyntaxHighlighting(syntaxHighlighting);
			metas.add(meta);
		}
		if(StrKit.notBlank(commentShortcuts)) {
			Usermeta meta = userService.getUsermeta(user.getID(), Usermeta.comment_shortcuts);
			if(null == meta) {
				meta = new Usermeta();
				meta.setUserId(user.getID());
			}
			meta.setCommentShortcuts(commentShortcuts);
			metas.add(meta);
		}
		if(StrKit.notBlank(adminBarFront)) {
			Usermeta meta = userService.getUsermeta(user.getID(), Usermeta.show_admin_bar_front);
			if(null == meta) {
				meta = new Usermeta();
				meta.setUserId(user.getID());
			}
			meta.setShowAdminBarFront(adminBarFront);
			metas.add(meta);
		}
		if(StrKit.notBlank(simpleLocalAvatar)) {
			Usermeta meta = userService.getUsermeta(user.getID(), Usermeta.simple_local_avatar);
			if(null == meta) {
				meta = new Usermeta();
				meta.setUserId(user.getID());
			}
			meta.setSimpleLocalAvatar(simpleLocalAvatar);
			metas.add(meta);
		}
		
		if(userService.update(user, metas)) {
			CacheKit.put(Users.loginUserCacheName, sessionId, user); // 更新登录用户缓存
			ok(Ret.by("msg", "更新个人资料成功").set("data", user.getID()));
		} else {
			fail("更新个人资料失败，请联系管理员");
		}
	}
	
}
